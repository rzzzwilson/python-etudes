# Drawing the gates

Now we want to start drawing some logic gates on the canvas.  We make this
pretty simple now by just creating `LogicGates` methods to draw
AND/NAND/OR/NOR/NOT gates on the canvas at a given position.  We will
call those methods in the `paintEvent()` method.

We also need some way of defining what gates we want and where we want them
drawn.  For simplicity we define a list of `(method, position)` tuples.
We call this sort of list a `draw list`:

    self.draw_list = [
                      (self.draw_and, (30, 30)),
                      (self.draw_or, (150, 60)),
                      (self.draw_nand, (250, 45)),
                      (self.draw_nor, (450, 250)),
                      (self.draw_not, (300, 50)),
                      (self.draw_and, (600, 400)),  # offscreen in initial view
                     ]

The methods that actually draw the gates are simple.  Here's the
method to draw a NOT gate:

    def draw_not(self, qp, x, y):
        """Draw NOT gate at posn (x, y)."""

        # basic outline
        qp.drawLine(x, y, x, y+20)
        qp.drawLine(x, y, x+20, y+10)
        qp.drawLine(x, y+20, x+20, y+10)
    
        # draw connections
        qp.drawLine(x-5, y+10, x, y+10)
        qp.drawEllipse(x-8, y+9, 2, 2)
    
        qp.drawEllipse(x+20, y+8, 4, 4)
        qp.drawLine(x+25, y+10, x+30, y+10)
        qp.drawEllipse(x+31, y+9, 2, 2)

This is reasonably simple.  The other gates are similar but a little larger.

The `drawWidget()` method has an added section to iterate through the draw
list and draw the specified gates:

        # draw objects
        pen = QPen(self.obj_colour, 1, Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        for (painter, args) in self.draw_list:
            painter(qp, *args)

The result of the code additions:

![the LogicGatesWidget](screen_1.png "Showing gates")

-----

This code is in the file
[PyQtCustomWidget-01.py](PyQtCustomWidget_01.py).
Run the code.  Note that one gate is drawn initially off-screen but by resizing
you can see it.

On [the next page](PyQtCustomWidget_02.md)
we need to highlight gates when the mouse hovers over them.
