#!/usr/bin/env python3

"""
Version 4 of the LogicGates custom widget.

Add highlighting of gate CONNECTIONS.

PyQtCustomWidget_04.mdWe also refactor the local classes a bit.  Made a superclass
GUIObject which is parent to all GUI objects.
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QColor, QPen, QPainterPath


Version = '0.4'


#######
# Base class for GUI objects (Connections and Gates and sub-children).
#######

class GUIObject:
    """Base class for objects drawn on the widget canvas."""

    def __init__(self, x, y, e_up, e_down, e_left, e_right):
        """Set connection position (x, y)."""

        self.x = x
        self.y = y
        self.e_up = e_up
        self.e_down = e_down
        self.e_left = e_left
        self.e_right = e_right

        self.highlight = False  # True if the object is highlighted

    def collision(self, x, y):
        """True if point (x, y) collides with object outline (x, y, w, h)."""

        return (x >= (self.x - self.e_left) and x < (self.x + self.e_right)
                and y >= (self.y - self.e_up) and y < (self.y + self.e_down))

    def move(self, dx, dy):
        """Move the object."""

        self.x += dx
        self.y += dy

#######
# Base class for a connection
#######

class Connection(GUIObject):

    BackgroundColour = Qt.white
    OutlineColour = Qt.black                # colour for connection outline
    SelectedColour = QColor(255, 200, 255)  # the "connection selected" fill colour
    OutlineWidth = 1                        # connection outline width

    # connection extent measures
    E_Left = 8
    E_Right = 8
    E_Up = 8
    E_Down = 8

    def __init__(self, x, y):
        """Set connection position (x, y)."""

        super().__init__(x, y, Connection.E_Left, Connection.E_Right,
                               Connection.E_Up, Connection.E_Down)

    def __repr__(self):
        """Return a descriptive string."""

        return f'Connection(x={self.x}, y={self.y})'

    def draw(self, qp):
        """Just draw the highlight since rest done in derived class."""

        # draw the highlight, if any
        if self.highlight:
            qp.setPen(QPen(Connection.BackgroundColour))
            qp.setBrush(Connection.SelectedColour)
            qp.drawEllipse(self.x - Connection.E_Left - 1,
                           self.y - Connection.E_Up,
                           Connection.E_Left + Connection.E_Right,
                           Connection.E_Up + Connection.E_Down)
            qp.setBrush(Connection.BackgroundColour)

        # draw the connection circle
        qp.setPen(QPen(Connection.OutlineColour, Connection.OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        qp.drawEllipse(self.x-2, self.y-1, 2, 2)

class Input(Connection):
    def __init__(self, x, y):
        super().__init__(x, y)

    def draw(self, qp):
        """Draw the connection onto the canvas.

        This is an input so line is drawn to the right.
        """

        # parent draws highlight and connection circle
        super().draw(qp)

        # draw input line to left of the circle
        qp.drawLine(self.x, self.y, self.x+6, self.y)

class Output(Connection):
    def __init__(self, x, y):
        super().__init__(x, y)

    def draw(self, qp):
        """Draw the connection onto the canvas.

        This is an output so line is drawn to the left.
        """

        # parent draws highlight and connection circle
        super().draw(qp)

        # draw input line to right of the circle
        qp.drawLine(self.x, self.y, self.x-6, self.y)

#######
# Base class for a gate
#######

class Gate(GUIObject):

    OutlineColour = Qt.black                # colour for gate outline
    SelectedColour = QColor(200, 255, 255)  # the "gate selected" fill colour
    OutlineWidth = 1                        # gate outline width

    def __init__(self, x, y, w, h):
        """Set gate position (x, y) and extent measures."""

        super().__init__(x, y, 0, w, 0, h)

        # define inputs/output lists of tuples for each input/output connection
        #     (dx, dy, w, h)
        # where (dx, dy) is connection offset from gate "zero" (top left corner)
        # and   (w, h)   is extent size around that connection point
        self.inputs = []
        self.output = None

    def move(self, dx, dy):
        """Move gate (and connections)."""

        # move the actual Gate
        super().move(dx, dy)

        # and then the connections associated with the Gate
        for input in self.inputs:
            input.move(dx, dy)
        self.output.move(dx, dy)

#######
# Derived (N)AND gate
#######

class GateAND(Gate):

    # gate body extent size
    Width = 20
    Height = 20

    def __init__(self, x, y, nand=False):
        """Derive from Gate, 'nand' is true if this is a NAND gate."""

        super().__init__(x, y, GateAND.Width, GateAND.Height)
        self.nand = nand
        self.inputs = [Input(x-6, y+5), Input(x-6, y+15)]
        if self.nand:   # must draw output connection differently if NAND
            self.output = Output(x+30, y+10)
        else:
            self.output = Output(x+26, y+10)

    def __repr__(self):
        """Return a descriptive string."""

        gate_name = 'NAND' if self.nand else 'AND'
        return f'{gate_name}(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the (N)AND gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.lineTo(self.x, self.y+20)                  # left side
        path.lineTo(self.x+10, self.y+20)               # bottom straight
        path.arcTo(self.x, self.y, 20, 20, 270, 180)    # curved right side
        path.closeSubpath()                             # back to (x, y)
        qp.drawPath(path)

        # draw inputs
        for inp in self.inputs:
            inp.draw(qp)

        # if NAND, draw negating output
        if self.nand:
            qp.drawEllipse(self.x+20, self.y+8, 4, 4)

        # draw output connection
        self.output.draw(qp)

#######
# Derived (N)OR gate
#######

class GateOR(Gate):

    # gate extent size
    Width = 20
    Height = 20

    def __init__(self, x, y, nor=False):
        """Derive from Gate, 'nor' is true if this is a NOR gate."""

        super().__init__(x, y, GateOR.Width, GateOR.Height)
        self.nor = nor
        self.inputs = [Input(x-5, y+5), Input(x-5, y+15)]
        if self.nor:    # must draw output connection differently if NOR
            self.output = Output(x+30, y+10)
        else:
            self.output = Output(x+26, y+10)

    def __repr__(self):
        """Return a descriptive string."""

        gate_name = 'NOR' if self.nor else 'OR'
        return f'{gate_name}(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the (N)OR gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.arcTo(self.x-48, self.y-15, 50, 50, 22, -44)   # left curved side
        path.arcTo(self.x-25, self.y-30, 50, 50, 270, 52)   # bottom right curved
        path.arcTo(self.x-25, self.y, 50, 50, 38, 52)       # top right curved
        path.closeSubpath()
        qp.drawPath(path)

        # draw inputs
        for inp in self.inputs:
            inp.draw(qp)

        # if NOR, draw negating output
        if self.nor:
            qp.drawEllipse(self.x+20, self.y+8, 4, 4)

        # draw output connection
        self.output.draw(qp)

#######
# Derived NOT gate
#######

class GateNOT(Gate):

    # gate extent size
    Width = 20
    Height = 20

    def __init__(self, x, y):
        """Derive from Gate."""

        super().__init__(x, y, GateNOT.Width, GateNOT.Height)
        self.inputs = [Input(x-6, y+10)]
        self.output = Output(x+30, y+10)

    def __repr__(self):
        """Return a descriptive string."""

        return f'NOT(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the NOT gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.lineTo(self.x, self.y+20)      # left edge
        path.lineTo(self.x+20, self.y+10)   # bottom slant
        path.closeSubpath()                 # back to (x,y)
        qp.drawPath(path)

        # draw the negation circle
        qp.drawEllipse(self.x+20, self.y+8, 4, 4)

        # draw input connectionss
        for input in self.inputs:
            input.draw(qp)

        # draw output connection
        self.output.draw(qp)

#######
# The custom widget displaying the defined gates.
#######

class LogicGates(QWidget):

    # definitions for the LogicGates widget
    GridSpacing = 50                    # spacing of grid lines
    GridColour = QColor(56, 56, 56)     # colour of the canvas grid lines
    OutlineColour = Qt.black            # the colour of outline around widget
    CanvasBG = Qt.white                 # the canvas background colour

    def __init__(self):
        super().__init__()              # initialize the underlying QWidget
        self.initUI()

        # set state variables
        self.highlighted_gate = None    # reference to highlighted gate
        self.dragged_gate = None        # reference to gate being dragged
        self.last_drag_x = 0            # previous X drag position
        self.last_drag_y = 0            # previous Y drag position

        self.show()

    def initUI(self):
        self.grid_spacing = LogicGates.GridSpacing

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), LogicGates.CanvasBG)
        self.setPalette(p)

        # start mouse tracking
        self.setMouseTracking(True)

        # create an initially empty list of objects to draw
        self.draw_list = []

    def add_gate(self, gate):
        """Add new gate to the handled gates."""

        self.draw_list.append(gate)

    def mousePressEvent(self, event):
        """Catch mouse button DOWN event."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # the button that was pressed
        button = event.button()

        # if left DOWN and gate highlighted, start dragging
        if button == Qt.LeftButton:
            if self.highlighted_gate:
                self.dragged_gate = self.highlighted_gate
                self.last_drag_x = mx
                self.last_drag_y = my
            if self.highlighted_connection:
                pass

#        elif button == Qt.RightButton:
#            pass
#        elif button == Qt.MidButton:
#            pass

    def mouseReleaseEvent(self, event):
        """Catch mouse button UP event."""

        # the button that was pressed
        button = event.button()

        # if left button up then stop dragging gate, if any
        if button == Qt.LeftButton:
            self.dragged_gate = None

#        elif button == Qt.RightButton:
#            pass
#        elif button == Qt.MidButton:
#            pass

    def mouseMoveEvent(self, event):
        """Catch a mouse move event.

        If dragging a gate, move it.

        If close enough to a gate highlight it.
        Only want to highlight one gate, so stop when first collision is found.
        """

        # get current mouse position
        mx = event.x()
        my = event.y()

        # reset all gate objects .highlight flag
        for obj in self.draw_list:
            obj.highlight = False
            for inp in obj.inputs:
                inp.highlight = False
            obj.output.highlight = False

        # handle dragging of gate
        if self.dragged_gate:
            # get delta move from last drag move
            delta_x = mx - self.last_drag_x
            delta_y = my - self.last_drag_y

            # update position of dragged gate
            self.dragged_gate.move(delta_x, delta_y)

            # update last drag move values
            self.last_drag_x = mx
            self.last_drag_y = my

        # check mouse position against extent of each Gate
        # and Connection object in the gate
        self.highlighted_gate = None
        self.highlighted_connection = None
        for obj in self.draw_list:
            if obj.collision(mx, my):
                obj.highlight = True    # so drawing object highlights it
                self.highlighted_gate = obj
                break

            for inp in obj.inputs:
                if inp.collision(mx, my):
                    inp.highlight = True
                    self.highlighted_connection = inp
                    break
            else:
                # if no inputs highlighted, check outputs
                if obj.output.collision(mx, my):
                    obj.output.highlight = True
                    self.highlighted_connection = inp
                    break

        # show changes to the view, if any
        self.repaint()

    def paintEvent(self, e):
        """Handle the widget 'paint' event.

        Check widget size, then draw draw objects on the canvas."""

        qp = QPainter()
        qp.begin(self)

        size = self.size()
        self.w = size.width()
        self.h = size.height()

        qp.setRenderHint(QPainter.Antialiasing)

        # draw grid
        pen = QPen(LogicGates.GridColour, 0.25, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(LogicGates.CanvasBG)
        for x in range(self.grid_spacing, self.w, self.grid_spacing):
            qp.drawLine(x, 0, x, self.h)
        for y in range(0, self.h, 50):
            qp.drawLine(0, y, self.w, y)

        # draw objects
        for o in self.draw_list:
            o.draw(qp)
            qp.setBrush(LogicGates.CanvasBG)

        # draw canvas outline
        pen = QPen(LogicGates.OutlineColour, 2, Qt.SolidLine)
        qp.setPen(pen)
        qp.drawLine(0, 0, 0, self.h)
        qp.drawLine(0, self.h, self.w, self.h)
        qp.drawLine(self.w, self.h, self.w, 0)
        qp.drawLine(self.w, 0, 0, 0)

        qp.end()

#######
# The test application used to demonstrate the custom widget
#######

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.gates = LogicGates()
        for gate in (
                     GateAND(30, 30),
                     GateOR(150, 160),
                     GateAND(250, 45, nand=True),
                     GateOR(450, 250, nor=True),
                     GateNOT(295, 90),
                     GateAND(600, 400),
                    ):
            self.gates.add_gate(gate)

        hbox = QVBoxLayout()
        hbox.addWidget(self.gates)

        self.setLayout(hbox)

        self.setGeometry(100, 100, 600, 400)
        self.setWindowTitle(f'LogicGates custom widget example {Version}')
        self.show()


app = QApplication(sys.argv)
ex = Example()
sys.exit(app.exec_())
