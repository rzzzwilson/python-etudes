# Prepare for our custom widget

Now we remove most of the code in the example and create a window containing
only our new `LogicGates` widget.

This new code just draws the white widget surface and the grid on it.  Note that
as we resize the application the grid is still drawn to match the size of the
widget surface.

The changes we make are very simple.  First, we remove the `Communicate()`
class which implements a signal used in the example.  We aren't going to use
signals in our widget so `Communicate()` and any use of it disappears.

Next, we change the name of the custom widget from `BurningWidget` to 
`LogicGates`.  The main change inside the class is to change the 
`drawWidget()` method to just draw a clean canvas with faint grid lines.

We also do simple little thing like adding a version number that is displayed
in the title of the window holding the widget, etc.

Run this and change the size of the window.  You will see the window is redrawn
properly, with the grid extending to the full size of the window.

![the blank LogicGates widget](screen_0.png "The Blank LogicGates Widget")

-----

This initial code is in the file
[PyQtCustomWidget-00.py](PyQtCustomWidget_00.py)

On
[the next page](PyQtCustomWidget_01.md)
we start to draw the gates.
