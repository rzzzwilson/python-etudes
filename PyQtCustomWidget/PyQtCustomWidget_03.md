# Dragging Gates

We want to be able to drag a gate around the canvas and position it where we
want.  This isn't hard, though it's a bit fiddly.

The first thing we have to change is that we want to see the mouse button
click events.  We don't want to know if a button was pressed and then released,
but we want to see the mouse button DOWN and UP events.  That's because we need
to see if we have a gate highlighted when we press the mouse left button DOWN.
If we get any mouse move events while the left button is down this means we 
should move the gate.

The way to see the mouse button events in PyQt is to override the QWidget base
class `mousePressEvent()` and `mouseReleaseEvent()` methods in our `LogicGates`
class.  We need some state data to handle dragging, and we set that up in the
`__init__()` method:

    self.dragged_gate = None        # reference to gate being dragged
    self.last_drag_x = 0            # previous X drag position
    self.last_drag_y = 0            # previous Y drag position

The `self.dragged_gate` value references the gate that is being dragged.  The 
`self.last_drag_x` and `self.last_drag_y` values are used to calculate how
much the mouse was moved since the last dragging move.  We update the gate's
position from those values.  Our `mousePressEvent()` method looks like:

    def mousePressEvent(self, event):
        """Catch mouse button DOWN event."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # the button that was pressed
        button = event.button()

        # if left DOWN and gate highlighted, start dragging
        if button == Qt.LeftButton:
            if self.highlighted_gate:
                self.dragged_gate = self.highlighted_gate
                self.last_drag_x = mx
                self.last_drag_y = my

The idea is that when the left mouse button is pressed **and a gate is
highlighted** then we start dragging that gate.  That means we set the
`self.dragged_gate` attribute to reference the highlighted gate.  Finally
we set the "previous" X and Y drag values to the current mouse position.
That's all we have to do here.

# Mouse move and drag

Our method triggered on a mouse move has to be changed to handle the case
when a gate is being dragged.  In addition to seeing if the cursor is over
a gate we need to check if we are dragging a gate and update the gate position
if so.  We know we are dragging a gate if `self.dragged_gate` is not `None`:

    # handle dragging of gate
    if self.dragged_gate:
        # get delta move from last drag move
        delta_x = self.last_drag_x - mx
        delta_y = self.last_drag_y - my

        # update position of dragged gate
        self.dragged_gate.x -= delta_x
        self.dragged_gate.y -= delta_y

        # update last drag move values
        self.last_drag_x = mx
        self.last_drag_y = my

All we have to do, if dragging, is to figure out the delta we moved since the
last move event and update the gate position accordingly.  The `self.repaint()`
at the end of the function will draw the gate in its new position.

# Mouse button UP

The `mouseReleaseEvent()` method handles the event that occurs when the left
mouse button is released.  It's very simple.  If the left button was released
and we were dragging then set `self.dragged_gate` to `None`:

    def mouseReleaseEvent(self, event):
        """Catch mouse button UP event."""

        # the button that was pressed
        button = event.button()

        # if left button up then stop dragging gate, if any
        if button == Qt.LeftButton:
            self.dragged_gate = None

-----

This code is in the file
[PyQtCustomWidget-03.py](PyQtCustomWidget_03.py).
Run the code.  Note that hovering over a gate fills the gate with a highlight
colour, and now when we press and hold the left mouse button we can drag a
gate around the canvas!.

On [the next page](PyQtCustomWidget_04.md)
we extend the highlighting to the input/output connections.  We want this
because when adding connections we want some indication of which connection
will be used.
