#!/usr/bin/env python3

"""
Version 0 of the LogicGates widget.

Just draws a white surface with a grid.
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QColor, QPen


Version = '0.0'


class LogicGates(QWidget):

    GridSpacing = 50

    def __init__(self):
        super().__init__()
        self.initUI()
        self.show()

    def initUI(self):
        self.grid_spacing = LogicGates.GridSpacing

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)

        # color of grid lines
        self.grid_colour = QColor(128, 128, 128)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        size = self.size()
        self.w = size.width()
        self.h = size.height()

        qp.setRenderHint(QPainter.Antialiasing)

        # draw grid
        pen = QPen(self.grid_colour, 0.25, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        for x in range(self.grid_spacing, self.w, self.grid_spacing):
            qp.drawLine(x, 0, x, self.h)
        for y in range(0, self.h, 50):
            qp.drawLine(0, y, self.w, y)


class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.gates = LogicGates()

        hbox = QVBoxLayout()
        hbox.addWidget(self.gates)

        self.setLayout(hbox)

        self.setGeometry(100, 100, 600, 400)
        self.setWindowTitle(f'LogicGates custom widget example {Version}')
        self.show()


def main():
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


main()
