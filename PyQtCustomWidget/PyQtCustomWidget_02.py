#!/usr/bin/env python3

"""
Version 2 of the LogicGates custom widget.

Implement highlighting of objects as the mouse cursor hovers over them.
We need to use classes for the AND/OR/NOT gates and draw with a path
that we can optionally fill if selected.  This makes drawing the highlight
much easier, since we can just draw *with a fill*.
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QColor, QPen, QPainterPath


Version = '0.2'


#######
# Base class for a gate
#######

class Gate:

    OutlineColour = Qt.black                # colour for gate outline
    SelectedColour = QColor(200, 255, 255)  # the "gate selected" fill colour
    OutlineWidth = 1                        # gate outline width

    def __init__(self, x, y, w, h):
        """Set gate position (x, y) and size (w, h)."""

        self.x = x
        self.y = y
        self.w = w
        self.h = h

        self.highlight = False  # True if the gate is highlighted

    def collision(self, x, y):
        """True if point (x, y) collides with gate outline (x, y, w, h)."""

        return (x >= self.x and x < (self.x + self.w)
                and y >= self.y and y < (self.y + self.h))


#######
# Derived (N)AND gate
#######

class GateAND(Gate):

    # gate extent size
    Width = 20
    Height = 20

    def __init__(self, x, y, nand=False):
        """Derive from Gate, 'nand' is true if this is a NAND gate."""

        super().__init__(x, y, GateAND.Width, GateAND.Height)
        self.nand = nand

    def __repr__(self):
        """Return a descriptive string."""

        gate_name = 'NAND' if self.nand else 'AND'
        return f'{gate_name}(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the (N)AND gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.lineTo(self.x, self.y+20)                  # left side
        path.lineTo(self.x+10, self.y+20)               # bottom straight
        path.arcTo(self.x, self.y, 20, 20, 270, 180)    # curved right side
        path.closeSubpath()                             # back to (x, y)
        qp.drawPath(path)

        # draw inputs
        qp.drawLine(self.x-6, self.y+5, self.x, self.y+5)
        qp.drawEllipse(self.x-8, self.y+4, 2, 2)

        qp.drawLine(self.x-6, self.y+15, self.x, self.y+15)
        qp.drawEllipse(self.x-8, self.y+14, 2, 2)

        # draw output, negated or not
        if self.nand:
            # draw NOT circle and connection
            qp.drawEllipse(self.x+20, self.y+8, 4, 4)
            qp.drawLine(self.x+24, self.y+10, self.x+26, self.y+10)
        else:
            qp.drawLine(self.x+20, self.y+10, self.x+26, self.y+10)
        qp.drawEllipse(self.x+26, self.y+9, 2, 2)

#######
# Derived (N)OR gate
#######

class GateOR(Gate):

    # gate extent size
    Width = 20
    Height = 20

    def __init__(self, x, y, nor=False):
        """Derive from Gate, 'nor' is true if this is a NOR gate."""

        super().__init__(x, y, GateOR.Width, GateOR.Height)
        self.nor = nor

    def __repr__(self):
        """Return a descriptive string."""

        gate_name = 'NOR' if self.nor else 'OR'
        return f'{gate_name}(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the (N)OR gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.arcTo(self.x-48, self.y-15, 50, 50, 22, -44)   # left curved side
        path.arcTo(self.x-25, self.y-30, 50, 50, 270, 52)   # bottom right curved
        path.arcTo(self.x-25, self.y, 50, 50, 38, 52)       # top right curved
        path.closeSubpath()
        qp.drawPath(path)

        # draw inputs
        qp.drawLine(self.x-6, self.y+5, self.x+1, self.y+5)
        qp.drawEllipse(self.x-8, self.y+4, 2, 2)

        qp.drawLine(self.x-6, self.y+15, self.x+1, self.y+15)
        qp.drawEllipse(self.x-8, self.y+14, 2, 2)

        # draw output, negated or not
        if self.nor:
            qp.drawEllipse(self.x+20, self.y+8, 4, 4)
            qp.drawLine(self.x+24, self.y+10, self.x+26, self.y+10)
        else:
            qp.drawLine(self.x+20, self.y+10, self.x+26, self.y+10)
        qp.drawEllipse(self.x+26, self.y+9, 2, 2)

#######
# Derived NOT gate
#######

class GateNOT(Gate):

    # gate extent size
    Width = 20
    Height = 20

    def __init__(self, x, y):
        """Derive from Gate."""

        super().__init__(x, y, GateNOT.Width, GateNOT.Height)

    def __repr__(self):
        """Return a descriptive string."""

        return f'NOT(x={self.x}, y={self.y}, w={self.w}, h={self.h})'

    def draw(self, qp):
        """Draw the NOT gate onto the canvas."""

        # set pen and fill colour, if required
        qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                       Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
        if self.highlight:
            qp.setBrush(super().SelectedColour)

        # basic outline
        path = QPainterPath()
        path.clear()
        path.moveTo(self.x, self.y)
        path.lineTo(self.x, self.y+20)      # left edge
        path.lineTo(self.x+20, self.y+10)   # bottom slant
        path.closeSubpath()                 # back to (x,y)
        qp.drawPath(path)

        # draw the single input
        qp.drawLine(self.x-6, self.y+10, self.x, self.y+10)
        qp.drawEllipse(self.x-8, self.y+9, 2, 2)

        # draw negated output
        qp.drawEllipse(self.x+20, self.y+8, 4, 4)
        qp.drawLine(self.x+24, self.y+10, self.x+27, self.y+10)
        qp.drawEllipse(self.x+27, self.y+9, 2, 2)


#######
# The custom widget displaying the defined gates.
#######

class LogicGates(QWidget):

    # definitions for the LogicGates widget
    GridSpacing = 50                    # spacing of grid lines
    GridColour = QColor(56, 56, 56)     # colour of the canvas grid lines
    OutlineColour = Qt.black            # the colour of outline around widget
    CanvasBG = Qt.white                 # the canvas background colour

    def __init__(self):
        super().__init__()              # initialize the underlying QWidget
        self.initUI()
        self.show()

    def initUI(self):
        self.grid_spacing = LogicGates.GridSpacing

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), LogicGates.CanvasBG)
        self.setPalette(p)

        # start mouse tracking
        self.setMouseTracking(True)

        # create an initially empty list of objects to draw
        self.draw_list = []

    def add_gate(self, gate):
        """Add new gate to the handled gates."""

        self.draw_list.append(gate)

    def mouseMoveEvent(self, event):
        """Catch a mouse move event. If close enough to a gate highlight it."""

        # get current mouse position
        mx = event.x()
        my = event.y()

        # check mouse position against extent of each object
        for obj in self.draw_list:
            obj.highlight = obj.collision(mx, my)

        self.repaint()  # show the highlighted gate(s), if any

    def paintEvent(self, e):
        """Handle the widget 'paint' event.

        Check widget size, then draw draw objects on the canvas."""

        qp = QPainter()
        qp.begin(self)

        size = self.size()
        self.w = size.width()
        self.h = size.height()

        qp.setRenderHint(QPainter.Antialiasing)

        # draw grid
        pen = QPen(LogicGates.GridColour, 0.25, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(LogicGates.CanvasBG)
        for x in range(self.grid_spacing, self.w, self.grid_spacing):
            qp.drawLine(x, 0, x, self.h)
        for y in range(0, self.h, 50):
            qp.drawLine(0, y, self.w, y)

        # draw objects
        for o in self.draw_list:
            o.draw(qp)
            qp.setBrush(LogicGates.CanvasBG)

        # draw canvas outline
        pen = QPen(LogicGates.OutlineColour, 2, Qt.SolidLine)
        qp.setPen(pen)
        qp.drawLine(0, 0, 0, self.h)
        qp.drawLine(0, self.h, self.w, self.h)
        qp.drawLine(self.w, self.h, self.w, 0)
        qp.drawLine(self.w, 0, 0, 0)

        qp.end()

#######
# The test application used to demonstrate the custom widget
#######

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.gates = LogicGates()
        for gate in (
                     GateAND(30, 30),
                     GateOR(150, 160),
                     GateAND(250, 45, nand=True),
                     GateOR(450, 250, nor=True),
                     GateNOT(295, 90),
                     GateAND(600, 400),
                    ):
            self.gates.add_gate(gate)

        hbox = QVBoxLayout()
        hbox.addWidget(self.gates)

        self.setLayout(hbox)

        self.setGeometry(100, 100, 600, 400)
        self.setWindowTitle(f'LogicGates custom widget example {Version}')
        self.show()


app = QApplication(sys.argv)
ex = Example()
sys.exit(app.exec_())
