# Highlighting inputs/outputs

Now we want to show a highlight around the input/output connections.  We need
this because later we will create connections between gate inputs and outputs
and that process will be similar to dragging, except that a left mouse click on
a highlighted gate **output** will start drawing a rubber-band line between that
output and the position of the cursor.  When the left button is released over
a gate **input** then a permanent connection will be drawn between the initial
and final inputs/outputs.

Highlighting an input or output connection is very similar to the highlighting
of gates.  One difference is that we have to highlight differently because
filling the small circle used to mark gate inputs and output will not be very
visible.  Input/output highlighting will draw a larger circle around the
input/output connection circle.

# Refactoring

Adding input/output highlighting resulted in a lot of duplicate code, so we
take the opportunity to refactor the classes that represent the GUI objects 
in the widget.

```mermaid
classDiagram
GUIObject <|-- Connection
GUIObject : __init__()
GUIObject : collision()
GUIObject : move()
Connection <|-- Input
Connection <|-- Output
Connection : __init__()
Connection : draw()
Input : draw()
Output : draw()
GUIObject <|-- Gate
Gate <|-- GateAND
Gate <|-- GateOR
Gate <|-- GateNOT
Gate : __init__()
Gate : move()
GateAND .. Input : 2
GateAND .. Output : 1
GateAND : draw()
GateOR .. Input : 2
GateOR .. Output : 1
GateOR : draw()
GateNOT .. Input : 1
GateNOT .. Output : 1
GateNOT : draw()
```

The arrows with a broad triangular head indicate a parent-child
relationship.

The dotted lines show that the upper class contains one or
more of the lower class instances.  The number associated with the arrow
shows the number contained within the owning class.

The method names shown in a class are explicitly called from the owning class
or a child class (via `super()`).

![the LogicGatesWidget](screen_4.png "Highlighting inputs & outputs")

-----

This code is in the file
[PyQtCustomWidget-04.py](PyQtCustomWidget_04.py).
Run the code.  We can still highlight and drag a gate.  Input/output
connections can be highlighted but we can't create connections yet.

On [the next page](PyQtCustomWidget_05.md)
we will draw "wires" between gate connections.
