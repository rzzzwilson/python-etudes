# Wrapping Up

We have come a long way and with not much code, really.  The final file
`PyQtCustomWidget_05.py` is less than 580 lines of python, which includes
comments and blank lines.  That really isn't a lot considering what the 
widget does.

We could include more features, of course.  In no particular order of
desirability or difficulty we could:

* "package" the widget to allow easy use in another project
* drag connections when dragging gates
* allow the user to draw connections where they want them, instead of just
  straight lines
* allow connections to join at a "junction"
* allow the widget to return a description of gates and the connections between
  which would allow other code to simulate the gate performance
* allow gates to be added and deleted by the user and mouse
* allow the user to delete a connection
* and many others

These are left as an exercise for the student!
