# Highlighting Gates

We eventually want to be able to drag a gate around the canvas to place it.
As a nice feature we would like to highlight a gate when the mouse hovers over
it.  This is some indication that the mouse will be dragged if the user
presses and holds the left mouse button.  To do this we need to keep track
of where the mouse is on our drawing canvas, and we do this by overriding
the PyQt `mouseMoveEvent()` method.  This allows the code to see what the X/Y
position of the mouse is on the canvas.

We also need to somehow decide that the current mouse position is hovering
over a gate, so we need to keep track of the **extent** of each gate.  Since we 
will be adding many other details to gates we take the time now to define a
`Gate` class which the specific AND/OR/NOT gates will override.  That class is:

    class Gate:
    
        OutlineColour = Qt.black                # colour for gate outline
        SelectedColour = QColor(200, 255, 255)  # the "gate selected" fill colour
        OutlineWidth = 1                        # gate outline width
    
        def __init__(self, x, y, w, h):
            """Set gate position (x, y) and size (w, h)."""
    
            self.x = x
            self.y = y
            self.w = w
            self.h = h
    
            self.highlight = False  # True if the gate is highlighted
    
        def collision(self, x, y):
            """True if point (x, y) collides with gate outline (x, y, w, h)."""
    
            return (x >= self.x and x < (self.x + self.w)
                    and y >= self.y and y < (self.y + self.h))

Not much is done in this parent class, just define some attributes and decide if
the position `(x, y)` is within the gate extent.  Most of the change is in the
derived classes.  Here's the `NOT` class:

    class GateNOT(Gate):
    
        # gate extent size
        Width = 20
        Height = 20
    
        def __init__(self, x, y):
            """Derive from Gate."""
    
            super().__init__(x, y, GateNOT.Width, GateNOT.Height)
    
        def __repr__(self):
            """Return a descriptive string."""
    
            return f'NOT(x={self.x}, y={self.y}, w={self.w}, h={self.h})'
    
        def draw(self, qp):
            """Draw the NOT gate onto the canvas."""
    
            # set pen and fill colour, if required
            qp.setPen(QPen(super().OutlineColour, super().OutlineWidth,
                           Qt.SolidLine, Qt.FlatCap, Qt.MiterJoin))
            if self.highlight:
                qp.setBrush(super().SelectedColour)
    
            # basic outline
            path = QPainterPath()
            path.clear()
            path.moveTo(self.x, self.y)
            path.lineTo(self.x, self.y+20)      # left edge
            path.lineTo(self.x+20, self.y+10)   # bottom slant
            path.closeSubpath()                 # back to (x,y)
            qp.drawPath(path)
    
            # draw the single input
            qp.drawLine(self.x-6, self.y+10, self.x, self.y+10)
            qp.drawEllipse(self.x-8, self.y+9, 2, 2)
    
            # draw negated output
            qp.drawEllipse(self.x+20, self.y+8, 4, 4)
            qp.drawLine(self.x+24, self.y+10, self.x+27, self.y+10)
            qp.drawEllipse(self.x+27, self.y+9, 2, 2)

We see the usual `super()` call to call the parent class initializer, as well
as a debug `__repr__()` method.

Most of the complexity is in the `draw()` method.  We change the drawing code
from that used previously.  Now we use a "path" drawing process.  This makes
highlighting easier because if the gate is to be highlighted all we have to do
is add a coloured `Brush` to the drawn path.  There is no extra drawing if
the gate is highlighted.  The gate needs an attribute `self.highlight` to
control whether the gate is highlighted or not.

The `paintEvent()` method in the `LogicGates` class is almost the same:

    # draw objects
    for o in self.draw_list:
        o.draw(qp)
        qp.setBrush(LogicGates.CanvasBG)

In the last line we remove any `Brush` colour that may have been used.

The other major change is that we now add the gates to the `LogicGates`
instance rather declaring the gates inside the class.

The final code running, showing a highlighted gate:

![the LogicGatesWidget](screen_2.png "Highlighted gate")

-----

This code is in the file
[PyQtCustomWidget-02.py](PyQtCustomWidget_02.py).
Run the code.  Note that hovering over a gate fills the gate with a highlight
colour.

On [the next page](PyQtCustomWidget_03.md)
we allow the gates to be dragged around the canvas.
