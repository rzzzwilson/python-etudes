# A PyQt Custom Widget

Following a question in the **/r/learnpython** subreddit, here is a short
tutorial on creating a PyQt custom widget to display objects in PyQt.  The
original question asked about drawing connected logic gates and perhaps
animating logic states with time.  This etude will concern itself with just
drawing and modifying a set of logic gates.

The design criteria might include:

* AND, NAND, OR, NOR and NOT gates
* Connections between inputs and outputs
* Add, modify or delete objects (gates and connections)
* Ability to select and modify internal data for each gate
* Ability to drag single or multiple gates and associated connections
* Set initial states of inputs
* Simulate logic changes with time

## Starting Point

Searching for simple tutorials on custom widgets in PyQt I came across
a tutorial [on the Zetcode site](https://zetcode.com) that I could use
as a starting point.  That tutorial was for PyQt5 and has been updated
to [a PyQt6 version](https://zetcode.com/pyqt6/customwidgets/) but this
etude uses PyQt5.  If you want to use PyQt6 this tutorial should still
work but you need to change some imports.

We will start with the PyQt5 version and remove most of the code before
starting to see gates on our screen.  The code for the Zetcode PyQt5
example is in *custom_widget.py*.

[The etude starts here](PyQtCustomWidget_00.md).
