#!/usr/bin/env python3

"""
Version 1 of the LogicGates widget.

Just draws hard-coded AND/NAND/OR/NOR/NOT gates.
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QColor, QPen

Version = '0.1'


class LogicGates(QWidget):

    GridSpacing = 50

    def __init__(self):
        super().__init__()
        self.initUI()
        self.show()

    def initUI(self):
        self.grid_spacing = LogicGates.GridSpacing

        self.setAutoFillBackground(True)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.white)
        self.setPalette(p)

        self.grid_colour = QColor(128, 128, 128)
        self.obj_colour = QColor(0, 0, 0)

        # create a temporary object to draw
        self.draw_list = [
                          (self.draw_and, (30, 30)),
                          (self.draw_or, (150, 60)),
                          (self.draw_nand, (250, 45)),
                          (self.draw_nor, (450, 250)),
                          (self.draw_not, (300, 50)),
                          (self.draw_and, (600, 400)),  # offscreen in initial view
                         ]

    def draw_not(self, qp, x, y):
        """Draw NOT gate at posn (x, y)."""

        # basic outline
        qp.drawLine(x, y, x, y+20)
        qp.drawLine(x, y, x+20, y+10)
        qp.drawLine(x, y+20, x+20, y+10)

        # draw connections
        qp.drawLine(x-5, y+10, x, y+10)
        qp.drawEllipse(x-8, y+9, 2, 2)

        qp.drawEllipse(x+20, y+8, 4, 4)
        qp.drawLine(x+25, y+10, x+30, y+10)
        qp.drawEllipse(x+31, y+9, 2, 2)

    def draw_and(self, qp, x, y):
        """Draw AND gate at posn (x, y)."""

        # basic outline
        qp.drawLine(x, y, x, y+20)
        qp.drawLine(x, y, x+10, y)
        qp.drawLine(x, y+20, x+10, y+20)
        qp.drawArc(x, y, 20, 20, 270 * 16, 180 * 16)

        # draw connections
        qp.drawLine(x-5, y+5, x, y+5)
        qp.drawEllipse(x-8, y+4, 2, 2)

        qp.drawLine(x-5, y+15, x, y+15)
        qp.drawEllipse(x-8, y+14, 2, 2)

        qp.drawLine(x+20, y+10, x+25, y+10)
        qp.drawEllipse(x+26, y+9, 2, 2)

    def draw_nand(self, qp, x, y):
        """Draw NAND gate at posn (x, y)."""

        # basic outline
        qp.drawLine(x, y, x, y+20)
        qp.drawLine(x, y, x+10, y)
        qp.drawLine(x, y+20, x+10, y+20)
        qp.drawArc(x, y, 20, 20, 270 * 16, 180 * 16)

        # draw connections
        qp.drawLine(x-5, y+5, x, y+5)
        qp.drawEllipse(x-8, y+4, 2, 2)

        qp.drawLine(x-5, y+15, x, y+15)
        qp.drawEllipse(x-8, y+14, 2, 2)

        qp.drawEllipse(x+20, y+8, 4, 4)
        qp.drawLine(x+24, y+10, x+29, y+10)
        qp.drawEllipse(x+30, y+9, 2, 2)

    def draw_or(self, qp, x, y):
        """Draw OR gate at posn (x, y)."""

        # basic outline
        qp.drawArc(x-48, y-15, 50, 50, 338*16, 45*16)
        qp.drawArc(x-25, y, 50, 50, 36*16, 54*16)
        qp.drawArc(x-25, y-30, 50, 50, 270*16, 54*16)

        # draw connections
        qp.drawLine(x-5, y+5, x+1, y+5)
        qp.drawEllipse(x-8, y+4, 2, 2)

        qp.drawLine(x-5, y+15, x+1, y+15)
        qp.drawEllipse(x-8, y+14, 2, 2)

        qp.drawLine(x+21, y+10, x+26, y+10) # moved 1 right
        qp.drawEllipse(x+26, y+9, 2, 2)

    def draw_nor(self, qp, x, y):
        """Draw NOR gate at posn (x, y)."""

        # basic outline
        qp.drawArc(x-48, y-15, 50, 50, 338*16, 45*16)
        qp.drawArc(x-25, y, 50, 50, 36*16, 54*16)
        qp.drawArc(x-25, y-30, 50, 50, 270*16, 54*16)

        # draw connections
        qp.drawLine(x-5, y+5, x+1, y+5)
        qp.drawEllipse(x-8, y+4, 2, 2)

        qp.drawLine(x-5, y+15, x+1, y+15)
        qp.drawEllipse(x-8, y+14, 2, 2)

        qp.drawEllipse(x+20, y+8, 4, 4)
        qp.drawLine(x+24, y+10, x+29, y+10)
        qp.drawEllipse(x+28, y+9, 2, 2)

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()

    def drawWidget(self, qp):
        size = self.size()
        self.w = size.width()
        self.h = size.height()

        qp.setRenderHint(QPainter.Antialiasing)

        # draw grid
        pen = QPen(self.grid_colour, 0.25, Qt.DotLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        for x in range(self.grid_spacing, self.w, self.grid_spacing):
            qp.drawLine(x, 0, x, self.h)
        for y in range(0, self.h, 50):
            qp.drawLine(0, y, self.w, y)

        # draw objects
        pen = QPen(self.obj_colour, 1, Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        for (painter, args) in self.draw_list:
            painter(qp, *args)

        # draw outline
        pen = QPen(self.obj_colour, 2, Qt.SolidLine)
        qp.setPen(pen)
        qp.setBrush(Qt.NoBrush)
        qp.drawLine(0, 0, 0, self.h)
        qp.drawLine(0, self.h, self.w, self.h)
        qp.drawLine(self.w, self.h, self.w, 0)
        qp.drawLine(self.w, 0, 0, 0)

class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.gates = LogicGates()

        hbox = QVBoxLayout()
        hbox.addWidget(self.gates)

        self.setLayout(hbox)

        self.setGeometry(100, 100, 600, 400)
        self.setWindowTitle(f'LogicGates widget example {Version}')
        self.show()


def main():
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


main()
