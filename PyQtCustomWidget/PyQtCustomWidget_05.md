# Drawing Connections

Now we want to add lines from a gate connection to another gate
connection.  Similar to dragging, on a left mouse DOWN event we check what
is highlighted.  If a Gate symbol is highlighted, we prepare for a Gate drag.
If nothing is highlighted, we do nothing.

But if a Connection symbol is highlighted we will prepare for drawing a
connection.  We note the Connection X and Y position and set a flag variable
`self.from_connection` to indicate that we are drawing a connection.  In the
`paintEvent()` method we draw the "rubberband" line from a selected `Input`
or `Output` connection to the current mouse position:

    def paintEvent(self, e):
        # old code

        # draw connections created
        pen = QPen(LogicGates.ConnectionColour, 1, Qt.DotLine)
        qp.setPen(pen)
        for line in self.connections:
            qp.drawLine(*line)

        # draw objects
        for o in self.draw_list:
            o.draw(qp)

        if self.from_connection:
            # draw connection from self.from_connection to current position
            pen = QPen(LogicGates.ConnectionColour, 1, Qt.DotLine)
            qp.setPen(pen)
            qp.drawLine(self.from_connection.x - 1, self.from_connection.y,
                        self.mouse_x, self.mouse_y + 1)

In the `mouseReleaseEvent()` handler code we check to see if we are drawing
a connection rubberband.  If we are, we also check if a connection (that
**isn't** the `from_connection` object) is highlighted.  If so, we add the
connection line to another state variable `self.connections`:

    if button == Qt.LeftButton:
        if self.from_connection:
            if (self.highlighted_connection
                and self.highlighted_connection is not self.from_connection):
                self.connections.append((self.from_connection.x - 1,
                                         self.from_connection.y,
                                         self.highlighted_connection.x - 1,
                                         self.highlighted_connection.y))

We also draw lines from the `self.connections` list that holds existing
connection lines.  See the `paintEvent()` code above.

The results of all this a widget that you can display and move gates and draw
connections between gates:


![Running PyQtCustomWidget-05.py](screen_5.png "Running PyQtCustomWidget-05.py")

-----

This code is in the file
[PyQtCustomWidget-05.py](PyQtCustomWidget_05.py).
Run the code.  We can still highlight and drag a gate.  Input/output
connections can be highlighted but we can't drag connections yet.

On [the next page](PyQtCustomWidget_06.md)
we wrap up this etude.
