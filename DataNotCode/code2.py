import random

class Dag:
    def __init__(self, func):
        self.func = func
        self.wastrue = None   # fill these in when defining the DAG
        self.wasfalse = None
 
# action functions, randomly return True or False
# use a generating function
def make_func(name):
    def func():
        result = random.choice((True, False))
        print(f"{name} returning {result}")
        return result
    return func

A = make_func("A")
B = make_func("B")
C = make_func("C")
D = make_func("D")
E = make_func("E")
F = make_func("F")
G = make_func("G")
H = make_func("H")

###################
# everything above here is just so the code will execute
###################

# function to "walk" a DAG
def walk(dag, index):
    while index is not None:
        (func, wastrue, wasfalse) = dag[index]
        next_func = wastrue if func() else wasfalse

        if next_func is None:
            return

        # find index of the next function
        for (i, elt) in enumerate(dag):
            if elt[0] is next_func:
                index = i
                break
        else:
            raise ValueError("Unrecognized function in DAG: {next_func}")
 
# define the DAG
#     tuple (A, B, C) means
#            |  |  |
#  function--+  |  +-- False link
#               +-- True link
dag = ((A, B, C),       # if A returns True goto B, else C
       (B, D, E),
       (C, E, F),
       (D, None, G),
       (E, None, H),
       (F, None, None),
       (G, None, None),
       (H, None, None),
      )

walk(dag, 0)    # start at tuple 0
