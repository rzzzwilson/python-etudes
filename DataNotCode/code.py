import random
 
class Dag:
    def __init__(self, func):
        self.func = func
        self.wastrue = None   # fill these in when defining the DAG
        self.wasfalse = None
 
# action functions, randomly return True or False
def A():
    result = random.random() < 0.5
    print(f"A returning {result}")
    return result
 
def B():
    result = random.random() < 0.5
    print(f"B returning {result}")
    return result
 
def C():
    result = random.random() < 0.5
    print(f"C returning {result}")
    return result
 
def D():
    result = random.random() < 0.5
    print(f"D returning {result}")
    return result
 
def E():
    result = random.random() < 0.5
    print(f"E returning {result}")
    return result
 
def F():
    result = random.random() < 0.5
    print(f"F returning {result}")
    return result
 
def G():
    result = random.random() < 0.5
    print(f"G returning {result}")
    return result
 
def H():
    result = random.random() < 0.5
    print(f"H returning {result}")
    return result
 
# everything above here is just so the code will execute
 
# function to "execute" a DAG
def walk(dag):
    while dag:
        dag = dag.wastrue if dag.func() else dag.wasfalse
 
# define the DAG
a = Dag(A)
b = Dag(B)
c = Dag(C)
d = Dag(D)
e = Dag(E)
f = Dag(F)
g = Dag(G)
h = Dag(H)
 
a.wastrue = b
a.wasfalse = c
b.wastrue = d
b.wasfalse = e
c.wastrue = e
c.wasfalse = f
d.wastrue = None    # None because wastrue is STOP
d.wasfalse = g
e.wastrue = None
e.wasfalse = h
f.wastrue = None
f.wasfalse = None
g.wastrue = None
g.wasfalse = None
h.wastrue = None
h.wasfalse = None
 
walk(a)
