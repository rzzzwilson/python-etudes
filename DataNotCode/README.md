# Data Not Code

A poster on reddit wanted to know if there was a better way
to define and process a directed acyclic graph that executes a
series of functions than using deeply nested if/else statements.

The particular scenario mentioned was a set of functions that
returned True or False.  If function A returned True then function
B was to be called.  If A returned False then function C was to be
called, and so on.  Suppose the relationship between each function
was something like this:

```mermaid
flowchart
    A[Function A] --> |true| B(Function B)
    A --> |false| C(Function C)
    B --> |true| D(Function D)
    B --> |false| E(Function E)
    C --> |true| E
    C --> |false| F(Function F)
    D --> |false| G(Function G)
    D --> |true| STOP
    E --> |true| STOP
    E --> |false| H(function H)
    F --> STOP
    G --> STOP
    H --> STOP
```

Note that some functions, like "Function E", don't have a 
function to call if the result is True.  In that case the 
walk just stops.  "Function F" has both true/false functions
undefined, so whatever the result the walk stops at that point.

The naive if/else execution approach would look
something like this in pseudo-code:

    if A():
        if B():
            if D():
                STOP
            else:
                G()
                STOP
        else:
            if E():
                STOP
            else:
                H()
                STOP
    else:
        if C():
            if E():
                STOP
            else:
                H()
                STOP
        else:
            F()
            STOP

Even for this simple case the nesting is getting out of hand.
Plus it's hard to make sure it's all correct.

# Use a data structure

There is another approach.  Define a DAG data structure and walk
the structure with code.  The DAG structure must represent the
logical structure shown in the figure above.  The challenge is to build
the structure in a readable and easily checkable way.

Once the structure is built we can easily write a function which
is given a reference to the node to start at and which walks through
the structure calling each function and then moving to the next
function to call depending on the result of the previous function
call.

# Some dummy "action" functions

First we define the action functions, A through H:

    import random

    # action functions, randomly return True or False
    def A():
        result = random.random() < 0.5
        print(f"A returning {result}")
        return result
     
    def B():
        result = random.random() < 0.5
        print(f"B returning {result}")
        return result
     
    def C():
        result = random.random() < 0.5
        print(f"C returning {result}")
        return result
     
    def D():
        result = random.random() < 0.5
        print(f"D returning {result}")
        return result
     
    def E():
        result = random.random() < 0.5
        print(f"E returning {result}")
        return result
     
    def F():
        result = random.random() < 0.5
        print(f"F returning {result}")
        return result
     
    def G():
        result = random.random() < 0.5
        print(f"G returning {result}")
        return result
     
    def H():
        result = random.random() < 0.5
        print(f"H returning {result}")
        return result

These functions just print their name and return a random True/False value.
The randomness in the return value means you can walk the structure twice
and possibly get different results.

# Defining the DAG structure

We create a `Dag` class that contains a reference to the function
to execute and a reference to the appropriate `Dag` to execute
depending on a True/False result.

    class Dag:
        def __init__(self, func, wastrue=None, wasfalse=None):
            self.func = func
            self.wastrue = None   # fill these in when defining the DAG
            self.wasfalse = None

First create all the DAG nodes:

    # define the DAG
    a = Dag(A)
    b = Dag(B)
    c = Dag(C)
    d = Dag(D)
    e = Dag(E)
    f = Dag(F)
    g = Dag(G)
    h = Dag(H)
    
Now we must fill in all those `.wastrue` and `.wasfalse` attributes
for each `Dag` node.  We do this in a verbose way but this is easily
checkable for correctness.

    a.wastrue = b
    a.wasfalse = c
    b.wastrue = d
    b.wasfalse = e
    c.wastrue = e
    c.wasfalse = f
    d.wastrue = None    # None because wastrue is STOP
    d.wasfalse = g
    e.wastrue = None
    e.wasfalse = h
    f.wastrue = None
    f.wasfalse = None
    g.wastrue = None
    g.wasfalse = None
    h.wastrue = None
    h.wasfalse = None

# The "walk" function 

Finally, we create the function that walks over the DAG calling the 
function in the current node and then moving to the true or false
node depending on the result of the function call:

    # function to "walk" a DAG given starting node
    def walk(node):
        while node:
            node = node.wastrue if node.func() else node.wasfalse

That's all there is.  The complete runnable code is in *code.py*.

Running this twice shows the effect of the random True/False returns:

    $ python code.py
    A returning False
    C returning True
    E returning False
    H returning True
    $ python code.py
    A returning False
    C returning False
    F returning True

# Enhancements

Creating the DAG structure is likely to be the hardest part, especially
for large graphs.  The approach above is easy to check but may not
be best for very large graphs.  If some data defining the graph is available
from somewhere you could write some python code to convert a representation
of that external data into the python code.  Or you could write a function
that reads some sort of external graph definition (file, database, etc)
and returns a completed graph.

If building the graph by hand there are a couple of things you can do to
catch errors:

* write code to check that every defined action function is used in the gtraph
* write code to check that there are no cycles in the graph

The code shown will handle cycles in the graph, but only because the result
of each action function is random.  If you action functions are deterministic
then you **cannot** have a cycle in the graph.

# Alternative data structures

We don't have to use the `Dag` OOP approach.  All we really need is
an object that holds three function references, like a tuple.  Then
we make a list of those tuples like this:

    dag = ((A, B, C),
           (B, D, E),
           (C, E, F),
           (D, None, G),
           (E, None, H),
           (F, None, None),
           (G, None, None),
           (H, None, None),
          )
           
A comparison between this and the DAG definition above will show that they
are logically equivalent.  The sequence of tuples is probably better at
defining the DAG structure, but the `walk()` function is more complicated.
It's a good exercise to try to make those changes yourself.  If you need
help the code to do the above is in `code2.py`.

# Wrap up

Here we show that it's often possible to use data to capture the logic
of a problem and thereby make the code simpler.
