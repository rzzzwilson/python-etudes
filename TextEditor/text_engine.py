"""
The text engine.

We just maintain a list of strings, each string being one line of text
that ends with a newline.

A text engine is an instance of the TextEngine class with methods to:

* read text from a file
* write text to a file
* maintain a cursor pointing to a place in a line
* delete N characters to the right of the 'cursor'
* delete N characters to the left of the 'cursor'
* delete N lines from current cursor position forward
* delete N lines from current cursor position backward
"""

import os

class TextEngine:

#    EOL = chr(0x2926)       # the graphics "newline" symbol
    EOL = chr(0x21D9)       # the graphics "newline" symbol

    def __init__(self, source=None):
        """Initialize the TextEngine, filling with text from 'source'.

        'source' is assumed to be a file path.  If no file of that name,
        'source' is assumed to be a string of initial text.
        """

        # file engine with text, maybe
        if source:
            self.read(source)
        else:
            self.text = []

        # set engine state to defaults
        self.default_state()

    def default_state(self):
        """Set state of engine to default values."""

        # describes cursor position in the engine
        self.cursor_line = 0
        self.cursor_offset = 0
        self.preferred_offset = 0

        # True if changed text in buffer has not been saved
        self.dirty = False

    def read(self, source):
        """Overwrite text in engine with text from given file.

        If file can't be opened, assume 'source' is text to use.
        """

        if os.path.isfile(source):
            # get text from file
            with open(source) as fd:
                data = fd.read()
            # add graphic newline to end of each line
            self.text = [line+TextEngine.EOL for line in data.splitlines()]
            # if last line has no newline, make it the same here
            if data[-1] != '\n':
                self.text[-1] = self.text[-1][:-1]
        else:
            self.text = [line+TextEngine.EOL for line in source.splitlines()]
            if source[-1] != '\n':
                self.text[-1] = self.text[-1][:-1]

        self.default_state()

    def save(self, path):
        """Save text in engine into file 'path'."""

        with open(path, 'w') as fd:
            for line in self.text:
                fd.write(line)

        self.dirty = False

    def del_char_left(self, n):
        """Delete N chars to the left of the cursor."""

        if n <= self.cursor_offset:
            line = self.text[self.cursor_line]
            new_line = line[:self.cursor_offset-n] + line[self.cursor_offset:]
            self.text[self.cursor_line] = new_line
            self.cursor_offset -= n
        else:
            new_line = self.text[self.cursor_line][self.cursor_offset:]
            self.text[self.cursor_line] = new_line
            self.cursor_offset = 0

        self.preferred_offset = self.cursor_offset

    def del_char_right(self, n):
        """Delete N chars to the right of the cursor."""

        if n <= len(self.text[self.cursor_line]) - self.cursor_offset:
            line = self.text[self.cursor_line]
            new_line = line[:self.cursor_offset] + line[self.cursor_offset+n:]
            self.text[self.cursor_line] = new_line
        else:
            line = self.text[self.cursor_line]
            new_line = line[:self.cursor_offset] + '\n'
            self.text[self.cursor_line] = new_line

        self.preferred_offset = self.cursor_offset

    def del_line_left(self, n):
        """Delete N lines to the left of the cursor."""

        pass

    def del_line_right(self, n):
        """Delete N lines to the right of the cursor."""

        pass

    def insert_left(self, text):
        """Insert 'text' to the left of the cursor."""

        line = self.text[self.cursor_line]
        new_line = line[:self.cursor_offset] + text + line[self.cursor_offset:]
        self.text[self.cursor_line] = new_line
        self.cursor_offset += len(text)

        self.preferred_offset = self.cursor_offset

    def insert_right(self, text):
        """Insert 'text' to the right of the cursor."""

        line = self.text[self.cursor_line]
        new_line = line[:self.cursor_offset] + text + line[self.cursor_offset:]
        self.text[self.cursor_line] = new_line

    def move_left(self, n):
        """Move cursor N places left."""

        self.cursor_offset = max(self.cursor_offset - n, 0)

        self.preferred_offset = self.cursor_offset

    def move_right(self, n):
        """Move cursor N places right."""

        self.cursor_offset = min(self.cursor_offset + n,
                                 len(self.text[self.cursor_line]))

        self.preferred_offset = self.cursor_offset

    def move_up(self, n):
        """Move cursor "n" lines up.

        Always try to move to self.preferred column.
        """

        if self.cursor_line > 0:
            self.cursor_line = max(self.cursor_line - n, 0)

            # move cursor so it's still on the text for this line, if necessary
            if self.cursor_offset > len(self.text[self.cursor_line]):
                self.cursor_offset = len(self.text[self.cursor_line])
            elif self.preferred_offset <= len(self.text[self.cursor_line]):
                # try to move cursor to "preferred" offset
                self.cursor_offset = self.preferred_offset

    def move_down(self, n):
        """Move cursor N lines down.

        Always try to move to self.preferred column.
        """

        if self.cursor_line < len(self.text) - 1:
            self.cursor_line =  min(self.cursor_line + n, len(self.text) - 1)
            if self.cursor_offset > len(self.text[self.cursor_line]):
                self.cursor_offset = len(self.text[self.cursor_line])
            else:
                if self.preferred_offset < len(self.text[self.cursor_line] - 1):
                    self.cursor_offset = self.preferred_offset

    def __str__(self):
        """Debug, print text in engine with cursor."""

        result = []
        for (i, line) in enumerate(self.text):
            if i == self.cursor_line:
                line = f'{line[:self.cursor_offset]}|{line[self.cursor_offset:]}'
            line = line.replace(TextEngine.EOL, '\n')
            result.append(line)
        return ''.join(result)


if __name__ == '__main__':
    buffer = TextEngine('test.txt')
#    buffer = TextEngine('alpha\nbeta\ngamma')
    print(str(buffer))
    print('------------------------------------------')
    buffer.move_right(200)
    print(str(buffer))
    print('------------------------------------------')
    buffer.move_down(1)
    print(str(buffer))
    print('------------------------------------------')
    buffer.move_down(1)
    print(str(buffer))
    print('------------------------------------------')
    buffer.move_up(2)
    print(str(buffer))
    print('------------------------------------------')
