TextEditor
==========

This etude will cover the design and implementation of a simple text editor
written in python.

Design
------

The editor will be written in three major parts:

* a "text engine" which will handle the external and internal storage of text along with changes to the text,
* a "display module" that will display the text on the screen as required, and
* the "control module" that will orchestrate operation of the editor, converting keyboard inputs into the appropriate text engine and display module calls.

Text Engine
-----------

The text engine must:

* read and write text from and to external files,
* implement primitive operations such as delete a character or line, insert text, etc,
* balance efficiency of text storage against the speed of primitive operations.

Look at various data structures that allow efficient text editing, like "ropes".

Initially we might just use a list of strings, with each line of text being one
string in the list.  Do we put a *newline* at the end of each string or just 
assume it's there?

Display Module
--------------

Initially this code will be absolutely minimal.  Just enough to handle commands
like:

* print from cursor to end of N lines, and
* print N lines back to the cursor.

The *N* parameter will be supplied by the control module.

Control Module
--------------

This code will handle the keyboard and any "macro" commands that are implemented.
