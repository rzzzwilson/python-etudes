from text_engine import TextEngine
import unittest

class TestTExtEngine(unittest.TestCase):

    def test_simple(self):
        te = TextEngine('some\ntest\ntext\n')
        expected_text = '|some\ntest\ntext\n'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_move(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(1)
        expected_text = 's|ome\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_move2(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.move_left(1)
        expected_text = 's|ome\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_move3(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.move_left(3)
        expected_text = '|some\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_move4(self):
        te = TextEngine('some\ntest\ntext')
        te.move_left(3)
        expected_text = '|some\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_move5(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(5)
        expected_text = 'some\n|test\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_del_left(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.del_char_left(1)
        expected_text = 's|me\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_del_left2(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.del_char_left(100)
        expected_text = '|me\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_del_right(self):
        te = TextEngine('some\ntest\ntext')
        te.del_char_right(2)
        expected_text = '|me\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_del_right2(self):
        te = TextEngine('some\ntest\ntext')
        te.del_char_right(200)
        expected_text = '|\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_del_right3(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.del_char_right(200)
        expected_text = 'so|\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_left(self):
        te = TextEngine('some\ntest\ntext')
        te.insert_left('TEST')
        expected_text = 'TEST|some\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_left2(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.insert_left('TEST')
        expected_text = 'soTEST|me\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_left3(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(4)
        te.insert_left('TEST')
        expected_text = 'someTEST|\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_right(self):
        te = TextEngine('some\ntest\ntext')
        te.insert_right('TEST')
        expected_text = '|TESTsome\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_right2(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(2)
        te.insert_right('TEST')
        expected_text = 'so|TESTme\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)

    def test_simple_insert_right3(self):
        te = TextEngine('some\ntest\ntext')
        te.move_right(4)
        te.insert_right('TEST')
        expected_text = 'some|TEST\ntest\ntext'
        text = str(te)
        msg = f"Text engine text be '{expected_text}', got '{text}'"
        self.assertEqual(text, expected_text, msg)


unittest.main()
