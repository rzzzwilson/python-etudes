def input_valid(prompt="", conv=str, isvalid=lambda x: True, errmsg="?"):
    """An abstracted version of "input()" that allows conversion, validation
    and retry if the validation fails.
    
    Arguments are:
        prompt   the the prompt string printed before getting input
        conv     a conversion function applied to the input string
        isvalid  a function to check if the result of "conv()" is valid
        errmsg   the string to print if the input is invalid
    
    The function returns the converted/validated object.
    """

    while True:
        in_str = input(prompt)
        try:
            result = conv(in_str)
        except ValueError:
            pass
        else:
            if isvalid(result):
                return result
        print(errmsg)
