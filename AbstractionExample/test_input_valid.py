from input_valid import input_valid

print(input_valid("Enter space-separated integers: ",
                  conv=lambda x: [int(i) for i in x.split()],
                  isvalid=lambda x: len(x)))

print(input_valid("Enter space-separated floats: ",
                  conv=lambda x: [float(i) for i in x.split()],
                  isvalid=lambda x: len(x)))

print(input_valid("Enter a float in [0,1): ", float, lambda x: 0 <= x < 1, "??"))

print(input_valid("Enter a positive integer: ", int, lambda x: x > 0,
                       "**** Sorry, only positive integers are allowed."))

print(input_valid("Enter an integer > 10: ", int, lambda x: x > 10))

print(input_valid("Enter an integer: ", conv=int))

print(input_valid("Name: ", isvalid=lambda x: len(x)))

print(input_valid())
