# Abstraction Example

A question on
[/r/learnpython](https://old.reddit.com/r/learnpython/new/)
asked why the `print()` function always returned a string and didn't allow any
interpretation of the input.  The answer, of course, is that this simplifies
the `input()` function.  If the caller wants to interpret the input string in
different ways they have to write code to do that.

It is possible to write a function, or functions, that interprets the input
string and returns the interpreted value or rejects it with a retry.

## First solution

The example used was a function that prompts the user for a list of integers
with a mechanism to retry if the input string isn't valid, containing
non-integers.  An empty input was considered as invalid.  The initial code was:

```
def input_int_list(prompt):
    while True:
        in_str = input(prompt)
        try:
            result = [int(x) for x in in_str.split()]
        except ValueError:
            pass
        else:
            if len(result):
                return result
        print("Sorry, you have to enter space-separated integers.")

print(input_int_list("Enter space-separated integers: "))
```

This works well and solves the original question.  But can we make it more
general?

## Abstraction 1

Suppose we want to also allow floats as the list type.  If that's all we wanted
to do we could change the function name, type conversion and error message:

```
def input_float_list(prompt):
    while True:
        in_str = input(prompt)
        try:
            result = [float(x) for x in in_str.split()]
        except ValueError:
            pass
        else:
            if len(result):
                return result
        print("Sorry, you have to enter space-separated floats.")

print(input_float_list("Enter space-separated floats: "))
```

But suppose we wanted one function to accept both integers or floats?  We can
do that by abstracting out some of the code and pass parameters that control
the type conversion and error message:

```
def input_num_list(prompt, conv, errmsg):
    while True:
        in_str = input(prompt)
        try:
            result = [conv(x) for x in in_str.split()]
        except ValueError:
            pass
        else:
            if len(result):
                return result
        print(errmsg)

print(input_num_list("Enter space-separated integers: ",
                     int,
                     "Sorry, you have to enter space-separated integers."))
print(input_num_list("Enter space-separated floats: ",
                     float,
                     "Sorry, you have to enter space-separated floats."))
```

That works a treat!  This is probably as far as you should go, but in the 
interests of studying abstration we are going to push this a bit further.

# Abstraction 2

The result of the function is a list of values.  That isn't what we always want
so maybe we can remove that assumption.  We again change the function name
and also treat the `conv` parameter as a function applied to the whole user
input string to produce whatever we want, list, tuple, integer, dictionary,
whatever.  We also need to abstract out the validation code because we are
no longer returning a list, so validation code also should be passed in:

```
def input_valid(prompt, conv, isvalid, errmsg):
    while True:
        in_str = input(prompt)
        try:
            result = conv(in_str)
        except ValueError:
            pass
        else:
            if isvalid(result):
                return result
        print(errmsg)

print(input_valid("Enter space-separated integers: ",
                  lambda x: [int(i) for i in x.split()],
                  lambda x: len(x),
                  "Only integers, please."))
print(input_valid("Enter space-separated floats: ",
                  lambda x: [float(i) for i in x.split()],
                  lambda x: len(x),
                  "Only floats, please."))
```

The test code shows the usual two test cases of asking for a list of integers or
floats, but we can do much more now:

```
print(input_valid("Enter a float in [0,1): ", float, lambda x: 0 <= x < 1, "??"))
print(input_valid("Enter a positive integer: ", 
                  int,
                  lambda x: x > 0,
                  "**** Sorry, only positive integers are allowed."))
print(input_valid("Enter an integer > 10: ", int, lambda x: x > 10, "??"))
print(input_valid("Enter an integer: ", int, lambda x: True, "??"))
```

#  Cleanup

The final form of the function should have keyword parameters with sensible
defaults:

```
def input_valid(prompt="", conv=str, isvalid=lambda x: True, errmsg="?"):
    while True:
        in_str = input(prompt)
        try:
            result = conv(in_str)
        except ValueError:
            pass
        else:
            if isvalid(result):
                return result
        print(errmsg)

print(input_valid("Enter space-separated integers: ",
                  conv=lambda x: [int(i) for i in x.split()],
                  isvalid=lambda x: len(x)))
print(input_valid("Enter space-separated floats: ",
                  conv=lambda x: [float(i) for i in x.split()],
                  isvalid=lambda x: len(x)))
print(input_valid("Enter a float in [0,1): ", float, lambda x: 0 <= x < 1, "??"))
print(input_valid("Enter a positive integer: ", int, lambda x: x > 0,
                       "**** Sorry, only positive integers are allowed."))
print(input_valid("Enter an integer > 10: ", int, lambda x: x > 10))
print(input_valid("Enter an integer: ", conv=int))
print(input_valid("Name: ", isvalid=lambda x: len(x)))
print(input_valid())
```

The additional test shows that the defaults are chosen to allow `input_valid()`
to behave much as `input()` does when not supplied with parameters: just accept
a string, even if it's empty.  The `errmsg=` default is a single "?" error
string, because the "ask again" nature of the function could be confusing
if there was no error message at all.

Note that supplying no arguments at all, or only a prompt string, makes the
function behave just like the standard library *input()* builtin function.

# Wrapup

This was an exercise in seeing how far we could push abstraction in a function
to get input values from the user.  The final code is probably **too** abstract
and shouldn't be used, with earlier, simpler abstractions being more readable
and useful.  But that's up to the programmer.
