#!/usr/bin/env python3

"""
Code to test the "legal_moves()" function in paths.py.
"""

import unittest
from paths import legal_moves, make_grid, print_grid

class TestLegalMoves(unittest.TestCase):

    def test_middle(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x//2, size_y//2)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(1, 1), (1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2), (3, 3)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_tlcorner(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (0, 0)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(0, 1), (1, 0), (1, 1)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_trcorner(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (0, size_y - 1)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(0, 3), (1, 3), (1, 4)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_blcorner(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x - 1, 0)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(3, 0), (3, 1), (4, 1)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_brcorner(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x - 1, size_y - 1)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(3, 3), (3, 4), (4, 3)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_topedge(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (0, size_y//2)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(0, 1), (0, 3), (1, 1), (1, 2), (1, 3)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_leftedge(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x//2, 0)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(1, 0), (1, 1), (2, 1), (3, 0), (3, 1)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_rightedge(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x//2, size_y - 1)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(1, 3), (1, 4), (2, 3), (3, 3), (3, 4)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
#        print()                     # DEBUG
#        print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")

    def test_bottomedge(self):
        size_x = 5
        size_y = 5
        grid = make_grid(size_x, size_y)

        # mark a square in exact middle and get legal moves
        posn = (size_x - 1, size_y//2)
        grid[posn[0]][posn[1]] = True
        lm = legal_moves(grid, posn)
        lm.sort()
        expected = [(3, 1), (3, 2), (3, 3), (4, 1), (4, 3)]
        expected.sort()
        for (x, y) in lm:
            grid[x][y] = True
 #       print()                     # DEBUG
 #       print_grid(grid, posn)      # DEBUG
        self.assertEqual(lm, expected, "Unexpected legal moves")


unittest.main()
