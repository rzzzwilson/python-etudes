# Paths

A question on
[/r/learnpython](https://old.reddit.com/r/learnpython/new/)
wanted to know how to solve a problem
with backtracking.  The problem was stated as:

```
Use backtracking to calculate the number of all paths from the bottom left
to the top right corner in a grid. Note that every point can only be visited
once. Write a function np(x,y) that returns the number of paths in a grid.
E.g. np(2,3) should return 38.

Hint: Create a grid of booleans where you mark the positions already visited.
```

## Initial thoughts

My initial thoughts were scribbled down over 5 or 10 minutes.  They are here:

![Initial thoughts](scribbles.jpg)

I have done this sort of path backtracking before, so this was a bit quicker
than normal.  I sent an initial semi-pseudo-code reply to the OP:

```
def legal_moves(grid, posn):
    """Return list of all possible moves from position "posn"."""

    # could be 4 moves from "posn", could be 8
    return []    # need to actually calculate the list to return

def move(grid, posn, finish):
    """Try all moves from current position."""

    if posn == finish:
        save_path()         # save something
        return
    moves = legal_moves(grid, posn) # return list of legal move positions
    for m in moves:         # this is why a list of legal moves is a good idea
        # this is the backtracking part
        # we don't need to copy the grid, just mark, try move, unmark
        mark(grid, m)       # mark grid position "m" with True
        move(grid, m, finish)
        unmark(grid, m)     # mark grid position "m" with False

def np(x, y):
    """Prints number of paths for grid of (x, y) dimensions."""

    make grid (X,Y) size, fill with False
    set "start" to (0,0)
    set "finish" to (x-1,y-1)
    create global empty list "paths"    # or a counter, or ... something

    mark(grid, start)                   # mark start square as "visited"
    move(grid, start, finish)

    check global "paths", print results
```

After another hour or so I had a working solution.  I took the time to add some 
comments, debug code and help when running from the command line.  The final
result is in file *paths.py*.

Changes that were made on the way to working code:

* Replaced the idea of a global result with the more normal "the recursive call
  returns the number of paths" idea,
* Legal moves were up to 8 from a square,
* Added a debug "print grid" function - very useful,
* Replaced mark/unmark functions with *one* function that takes a True/False
  parameter,
* Put the "make grid" code into its own function - better for testing,
* "Productized" the code, command line execution, etc.

## Testing

This should have been done while writing the original code, but was actually
done afterward.  Tested only the *legal_moves()* function since I couldn't think
of a simple way to test anything else!

The test code is in *test_legal_moves.py* and uses the *unittest* module.

### Thoughts on the problem

I don't have access to the text the OP said the problem was from:

[Conceptual Programming with Python](https://www.amazon.com/Conceptual-Programming-Python-Thorsten-Altenkirch/dp/024482276X/ref=sr_1_1?crid=2E612P8A2P636&dchild=1&keywords=conceptual+programming+with+python&qid=1628150917&s=books&sprefix=Conceptual+Programming+with+Python%2Caps%2C441&sr=1-1)
by Thorsten Altenkirch and Isaac Triguero.

The problem, as stated by the OP, was supposed to have this solution: *np(2,3)
should return 38*.  However, my solution returns 24 for np(2,3)!?
I'm moderately sure my code is correct.  Maybe the actual problem in the book
is misquoted by the OP?
