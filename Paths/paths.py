#!/usr/bin/env python3

"""
A program to calculate the number of paths through a rectangular
grid from one corner to the opposite corner.  Call it like this:

    $ paths.py <Xsize> <Ysize> [debug]

where <Xsize> are <Ysize> are integer values and "debug" may
be any parameter.  This turns on debug printing.  For example:

    $ test.py 2 3 D
    X\Y 0 1 2
      +------
     0| X . .
     1| . . .
    posn=(0, 0), moves=[(0, 1), (1, 0), (1, 1)]

    <SNIP>

    X\Y 0 1 2
      +------
     0| x . .
     1| . x X
    posn=(1, 2), moves=[(0, 1), (0, 2)]

    SOLUTION!

    Number of paths for 2x3 grid = 24

"""

import sys

def legal_moves(grid, posn):
    """Return list of all possible moves from position "posn".

    Allow moves in any of the 8 cardinal directions.
    """

    result = []

    # test all squares in 3x3 area centred on "posn"
    (x, y) = posn
    for dx in [-1, 0, +1]:
        new_x = x + dx
        if new_x < 0 or new_x >= len(grid):
            continue            # ignore off-grid X
        for dy in [-1, 0, +1]:
            new_y = y + dy
            if new_y < 0 or new_y >= len(grid[0]):
                continue        # ignore off-grid Y
            if (dx,dy) == (0,0):
                continue        # ignore current position
            # if possible position unvisited, add to result list
            if not grid[new_x][new_y]:
                result.append((new_x, new_y))

    return result

def mark(grid, posn, value):
    """Set value at position in grid."""

    (x, y) = posn
    grid[x][y] = value

def print_grid(grid, posn):
    """Debug function.  Print grid in a nice way.  Show current position:

        X\Y 0 1 2
          +------
         0| x . .
         1| . x X

    Where "X" is the current position, and "x" is a visited position.
    """

    # first row - Y coordinate
    print('X\\Y ', end='')
    for y in range(len(grid[0])):
        print(f'{y} ', end='')
    print()

    # second row - delimiter
    print('  +', end='')
    for y in range(len(grid[0])):
        print(f'--', end='')
    print()

    # print all grid rows with left side coords and delimiter
    for x in range(len(grid)):
        print(f'{x:2d}| ', end='')
        for y in range(len(grid[0])):
            if posn == (x,y):
                print('X ' if grid[x][y] else '. ', end='')
            else:
                print('x ' if grid[x][y] else '. ', end='')
        print()

def move(grid, posn, finish):
    """Try all moves from current position."""

    # get list of legal moves from current position on grid
    moves = legal_moves(grid, posn)

    if DEBUG:
        print_grid(grid, posn)
        print(f'posn={posn}, moves={moves}')
        print()

    if posn == finish:
        if DEBUG:
            print('SOLUTION!')
            print()
        return 1                # CHANGED for recursive sum

    # set result (# of paths) for this recursive call
    result = 0                  # CHANGED for recursive sum

    # try all possible moves, with backtracking
    for m in moves:
        # this is the backtracking part
        # we don't need to copy the grid, just mark used, try move, mark unused
        mark(grid, m, True)     # mark grid position "m" with True
        result += move(grid, m, finish) # CHANGED for recursive sum
        mark(grid, m, False)    # mark grid position "m" with False

    return result

def make_grid(x, y):
    """Make a grid nested list with (x,y) dimensions."""

    # create grid with size (X,Y), fill with False
    grid = []
    for col in range(x):
        row_list = []
        for row in range(y):
            row_list.append(False)
        grid.append(row_list)

    return grid

def np(x, y):
    """Prints number of paths for grid of (x, y) dimensions."""

    # create the board grid, filled with False
    grid = make_grid(x, y)

    # create "start" and "finish" positions
    start = (0, 0)
    finish = (x-1, y-1)

    # mark start and call recursive solver
    mark(grid, start, True)
    return move(grid, start, finish)    # CHANGED for recursive sum

if __name__ == '__main__':
    # get the X and Y sizes from command line
    if len(sys.argv) < 3:
        print(__doc__)
        sys.exit(10)

    size_x = int(sys.argv[1])
    size_y = int(sys.argv[2])

    # set DEBUG if user passed any params on commandline
    DEBUG = len(sys.argv) > 3

    paths = np(size_x, size_y)
    print(f'Number of paths for {size_x}x{size_y} grid = {paths}')
