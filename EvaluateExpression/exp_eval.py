"""
Code to implement a simple expression evaluator.  This question, and related
questions, comes up on /r/learnpython subreddit occasionaly.  The post that
prompted this code is: https://redd.it/kjf7qc .

Enter expressions in python syntax: 3*a**2 -5*b + c
Undefined values ('a', 'b' and 'c' in the above example) will be prompted for.
Defined variables maintained between expressions.

Some direct commands are allowed:
    quit            exit the program
    show [name]     show the named variable value, all variables if no name
    del name        delete the named variable
    delall          delete ALL defined variables

This example uses eval() so it should not be used where untrusted strings
can be entered for expressions.
"""

local_vars = {}     # the variables defined by the user
 
while True:
    # get an expression
    expression = input('Expression: ')

    # if it's "quit" finish up
    if expression.lower() == 'quit':
        break

    # if it starts with "del" try to delete one or all locals
    if expression.lower().startswith('del'):
        fields = expression.split()
        if len(fields) == 1:
            if fields[0].lower() == 'delall':
                local_vars = {}
            else:
                print('Sorry, you must include the name to delete')
        elif len(fields) == 2:
            name = fields[1].strip()
            if name in local_vars:
                del local_vars[name]
            else:
                print(f"Sorry, name '{name}' isn't defined")
        else:
            print('Sorry, bad command\n')
        print()
        continue

    # if it's "show" print all known values
    if expression.lower().startswith('show'):
        fields = expression.split()
        if len(fields) == 1:
            print('Vars:', end='')
            for (k, v) in local_vars.items():
                print(f'\t{k} = {v}')
            else:
                print()
        elif len(fields) == 2:
            name = fields[1].strip()
            if name in local_vars:
                print(f'Vars:\t{name} = {local_vars[name]}')
            else:
                print(f"Sorry, name '{name}' isn't defined")
        else:
            print('Sorry, bad command\n')
        print()
        continue

    # if the expression contains "=" treat it as an assignment
    if '=' in expression:   # if so assume assignment
        (name, expression) = expression.split('=', maxsplit=1)
        try:
            local_vars[name] = eval(expression, {}, local_vars)
        except SyntaxError:
            print('Sorry, the value to assign is invalid')
            continue
        except NameError:
            # we don't allow any undefined names here
            print('Sorry, the value to assign is undefined')
            continue
        print(f'local_vars={local_vars}')
        continue

    # otherwise it's an expression
    var_names = []      # holds variables undefined in the expression
    retry = True        # set to False when we want to 'break' to outer loop
    while retry:
        result = None
        try:
            result = eval(expression, {}, local_vars)
        except SyntaxError:
            print('Sorry, that expression is invalid')
            retry = False       # we want to get a new expression
            continue
        except NameError as e:  # something is undefined
            print(f'Sorry, {e}')
            name = str(e).split("'")[1]             # get the undefined name
            value = input(f"Value for '{name}': ")
            local_vars[name] = eval(value)          # put name+value into 'local_vars'
            var_names.append(name)                  # remember names defined for this expression
            continue
 
        print(f"\nResult of '{expression}' is {result}\n")
        break
