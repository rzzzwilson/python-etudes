"""
Code to implement a simple expression evaluator.  This question, and related
questions, comes up on /r/learnpython subreddit occasionally.  The post that
prompted this code is: https://redd.it/kjf7qc .

Enter a valid expression in python syntax: 3 * (1 + 2)

This example uses eval() so it should not be used where untrusted strings
can be entered for expressions.
"""

# get an expression and evaluate it
expression = input('Expression: ')
result = eval(expression)
print(f"\nResult of '{expression}' is {result}\n")
