# Player pick up

We now want to allow the player to pick up and drop objects in the map.  We
already have objects at Places in the map, so now we must:

* Add an "inventory" to the player attributes
* Add "pickup" and "drop" commands to the allowed commands

## Player object

Up to this point we haven't needed a Player object.  The only thing connected
to the Player was the player position and we kept track of that with the global
*current_place*.  We'll keep *current_place* as a global but now we should
create the *Player* object, along the same lines as we did with Place.  We only
need to have attributes *.name* and *.inventory* at the moment:

    class Player:
        """An object to hold player information."""
    
        def __init__(self, name):
            self.name = name
            self.inventory = []     # holder for objects in player inventory
    
        def __str__(self):
            """For debug."""
    
            return f"Player('{self.name}')"

We keep the unique string identifier for each object in the Player inventory
just as we did for Objects in Places.

We also gave the Player a name which we don't use at the moment.

## Pickup/Drop commands

We need to add the "pickup" and "drop" commands to the allowed commands, along
with various aliases.  We also need some way of the player listing what s/he
holds, so we add the "inventory" command and aliases:

    allowed_commands = {'north': 'north', 'n': 'north',
                        'northeast': 'northeast', 'ne': 'northeast',
                        'east': 'east', 'e': 'east',
                        'southeast': 'southeast', 'se': 'southeast',
                        'south': 'south', 's': 'south',
                        'southwest': 'southwest', 'sw': 'southwest',
                        'west': 'west', 'w': 'west',
                        'northwest': 'northwest', 'nw': 'northwest',
                        'quit': 'quit', 'q': 'quit', 'ex': 'quit', 'exit': 'quit',
                        'stop': 'quit', 'leave': 'quit',
                        'look': 'look', 'l': 'look',
                        'get': 'get', 'g': 'get', 'pickup': 'get',
                        'drop': 'drop', 'd': 'drop',
                        'inventory': 'invent', 'inv': 'invent', 'i': 'invent',
                       }

## Doing new commands

We now have to decide how we are going to handle the new commands.  We could
handle them in the main loop at the end of the code, but it's probably best if
we do what we can in the *do_command()* function.  So that becomes:

    def do_command(verb, noun=None):
        global current_place
    
        # run through the list of action words
        if verb == 'get':
            # user wants to get something
            if noun is None:
                print("Sorry, you must use 'get' with a noun, like 'get axe'.")
            else:
                pickup_object(noun)
        elif verb == 'drop':
            # user wants to drop something
            if noun is None:
                print("Sorry, you must use 'drop' with a noun, like 'drop axe'.")
            else:
                drop_object(noun)
        elif verb == 'invent':
            if noun:
                # can't use a noun with "inventory"
                print("Sorry, the 'inventory' command doesn't take a second word.")
            else:
                inventory()
        else:
            # might be a move
            if verb in current_place.connections:
                current_place = place_name_ref[current_place.connections[verb]]
                push_prev(current_place)

## Testing

And that's all we have to do.  TIme to see if we can pickup and move that
elvish axe!:

    $ python3 TextAdventure.03.py 
    You are at the White house. Paths lead south and east from here.
    What do you want to do? east
    
    You are on a narrow east-west path.
    What do you want to do? e
    
    You are in a shadowed glade, with paths to the west and southwest.
    
    You see here:
    	a small Elvish axe
    What do you want to do? get axe
    Taken.
    
    You are in a shadowed glade, with paths to the west and southwest.
    What do you want to do? sw
    
    You are in a dark difficult forest. Narrow tracks go northeast and north.
    What do you want to do? n
    
    You are at the White house. Paths lead south and east from here.
    What do you want to do? drop axe
    Dropped.
    
    You are at the White house. Paths lead south and east from here.
    
    You see here:
    	a small Elvish axe
    What do you want to do? e
    
    You are on a narrow east-west path.
    What do you want to do? w
    
    You are at the White house.
    
    You see here:
    	a small Elvish axe

It works!

The
[code is here](TextAdventure.03.py)
and
[on the next page](TextAdventure.04.md)
we allow the map to contain monsters.
