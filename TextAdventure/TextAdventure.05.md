# Saving to a file

Now we want to think about how we save and restore a game on disk.

Initially we will just try to save a game to disk in some sort of readable
form.

## Writing data

We need to write all the state data to a "save" file.  We do that by first
moving all the textual state data into one place in the program.  This will
help later when we want to handle initializing the game if no save file is found.
The textual data consists of the *Player* instance, as well as those for
*Place*, *Object* and *Monster* instances.  We include the *current_place* value
and the *allowed_commands* dictionary.

We will write the data to file as JSON data.  This allows easy checking of
the saved data.

We create a function *save_state()* to write the data to disk:

    def save_state(fname):
        """Save state to the 'fname' file.
    
        The code first creates dictionaries of data which is saved as JSON.
        """
    
        # save Player, Place, Object and Monster data
        state = {'current_place': None,
                 'Player': {}, 'Place': [], 'Object': [], 'Monster': [],
                 'allowed_commands': {}}
    
        for (obj_name, obj) in globals().items():
            if obj_name == 'current_place':
                state['current_place'] = obj.name
            elif obj_name == 'allowed_commands':
                state['allowed_commands'] = obj
            elif isinstance(obj, Player):
                state['Player'] = obj.state()
            elif isinstance(obj, Place):
                state['Place'].append(obj.state())
            elif isinstance(obj, Object):
                state['Object'].append(obj.state())
            elif isinstance(obj, Monster):
                state['Monster'].append(obj.state())
    
        # save state as JSON
        with open(fname, 'w') as fd:
            fd.write(json.dumps(state, indent=4))

Note that we create a "state" dictionary holding all data we are going to save.
This helps when we restore data as we can recreate data in any order we require.
It also helps us add extra data such as a version number for the saved data, or
even comments.

The code is not too tricky.  We basically just cycle through all global
objects looking for the two "standard" variables *current_place* and
*allowed_commands* or an instance of the four classes defining entities in the
game: *Player*, *Place*, *Object* or *Monster*.  For the instance
objects we just append the dictionary describing the instance state.  We added
this method to all game object classes.  As an example, here is part of the
*Player* class:

    class Player:
        """An object to hold player information."""
    
        def __init__(self, name, inventory=None):
            self.name = name
            if inventory is None:
                inventory = []
            self.inventory = inventory
    
        def state(self):
            """Return a "dictionary" description of the Player.
    
            Just enough to recreate the Player from saved data.
            """
    
            return {'name': self.name,
                    'inventory': self.inventory,
                   }

Once we have filled the *state* dictionary, we write it to the save file as
JSON data.

## Testing

Testing is just a matter of playing the game to get into particular
circumstances, such as the player holding the axe, a monster in the same place
as the player, the player being in a place with some object, etc.  Quit the game
and look at the save JSON file.

As an example, here are details of a short episode in the game:

    $ python3 TextAdventure.05.py
    You are at the White house. Paths lead south and east from here.
    What do you want to do? e
    
    You are on a narrow east-west path.
    What do you want to do? e
    
    You are in a shadowed glade, with paths to the west and southwest.
    
    You see here:
    	a small Elvish axe.
    What do you want to do? get axe
    Taken.
    
    You are in a shadowed glade, with paths to the west and southwest.
    What do you want to do? w
    
    You are on a narrow east-west path.
    
    You see:
    	A hairy goblin with very bad breath.
    What do you want to do? q
    So long.

The *state.json* file produced when we exited the game is:

    {
        "current_place": "path",
        "Player": {
            "name": "Fred",
            "inventory": [
                "axe"
            ]
        },
        "Place": [
            {
                "name": "white_house",
                "description": "at the White house.",
                "connections": {
                    "east": "path",
                    "south": "forest"
                },
                "long_description": "at the White house. Paths lead south and east from here."
            },
            {
                "name": "path",
                "description": "on a narrow east-west path.",
                "connections": {
                    "east": "glade",
                    "west": "white_house"
                },
                "long_description": "on a narrow east-west path."
            },
            {
                "name": "glade",
                "description": "in a shadowed glade.",
                "connections": {
                    "west": "path",
                    "southwest": "forest"
                },
                "long_description": "in a shadowed glade, with paths to the west and southwest."
            },
            {
                "name": "forest",
                "description": "in a dark difficult forest.",
                "connections": {
                    "northeast": "glade",
                    "north": "white_house"
                },
                "long_description": "in a dark difficult forest. Narrow tracks go northeast and north."
            }
        ],
        "Object": [
            {
                "name": "axe",
                "description": "a small Elvish axe.",
                "place": null,
                "long_description": "a small Elvish axe. There are faint unreadable engravings on the head."
            }
        ],
        "Monster": [
            {
                "name": "goblin",
                "description": "A hairy goblin with very bad breath.",
                "place": "path"
            }
        ],
        "allowed_commands": {
            "north": "north",
            "n": "north",
            "northeast": "northeast",
            "ne": "northeast",
            "east": "east",
            "e": "east",
            "southeast": "southeast",
            "se": "southeast",
            "south": "south",
            "s": "south",
            "southwest": "southwest",
            "sw": "southwest",
            "west": "west",
            "w": "west",
            "northwest": "northwest",
            "nw": "northwest",
            "quit": "quit",
            "q": "quit",
            "ex": "quit",
            "exit": "quit",
            "stop": "quit",
            "leave": "quit",
            "look": "look",
            "l": "look",
            "get": "get",
            "g": "get",
            "pickup": "get",
            "drop": "drop",
            "d": "drop",
            "inventory": "invent",
            "inv": "invent",
            "i": "invent",
            "wait": "wait"
        }
    }

Careful examination shows that everything is as it should be:

* Player, Place, Object and Monster instances are correctly defined,
* The Player is in the correct Place,
* The Player has the Axe in her inventory, and
* The Monster is in the correct place.

## Next

The
[code is here](TextAdventure.05.py)
and you should test it yourself.

[On the next page](TextAdventure.06.md)
we try to restore the game state.
