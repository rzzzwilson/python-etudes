# Text Adventure

Now we add a few small features and one important one.

## The "quit" command

One thing missing from our game is a way to stop playing.  We add a "quit"
command so the player can stop cleanly.  The handling of the quit command is
also a good place to do things like saving the current game state to disk when
we get around to doing that.

To add the "quit" command we just need to add a few entries to the
*allowed_commands* dictionary:

    'quit': 'quit', 'q': 'quit', 'ex': 'quit', 'exit': 'quit',
    'stop': 'quit', 'leave': 'quit',

We add a few aliases for "quit" as there's nothing more frustrating to a player
than not being able to quit a game.

We also need to change the main loop to handle the "quit" command:

    while True:
        describe_place(current_place)
        cmd = get_command()
        if cmd == 'quit':
            break
        if not do_command(cmd):
            print("Sorry, you can't do that.  Try again.\n")
    
    print('So long.')

## Short and long Place descriptions

One feature that some adventures have that is nice and not hard to implement
is a long and short description of a Place.  When the player goes to a
Place they haven't visited before, they get shown the long description.  After
moving away and returning a short time later the short description of the
Place is shown to the player.  This is just a nice thing to implement.

We have to add the *long_description* attribute to the Place class:

    class Place:
        """A Place in the text adventure."""
    
        def __init__(self, name, description, connections, long_description=None):
            self.name = name
            self.description = description
            self.connections = connections
            self.long_description = description     # just in case no long description
            if long_description:
                self.long_description = long_description

We use a global list *previous_places* to remember the last N Places we have
visited, where N is a configurable value.  As we move around we keep
*previous_places* up to date.  When printing a Place description the code checks
if the current Place is the *previous_places[1:]*.  If so, the short description
is printed for the player, else the long description.

This requires changes to the *describe_place()* function, which is now a little
more complicated:

    def describe_place(place):
        if place not in previous_places[1:]:
            print('You are ' + place.long_description)
        else:
            print('You are ' + place.description)

The only other change that needs to be made is in the *do_command()* function,
where we must maintain *previous_places*.  We do this with a small helper
function:

    def push_prev(place):
        previous_places.insert(0, place)        # new place inserted at left
        del previous_places[num_previous:]

This function pushes the Place to the front of the *previous_places* list
and then limits the list to length *num_previous*.  In the *do_command()*
function we call *push_prev()* to push the current Place to the
*previous_places* list:

    def do_command(cmd):
        if cmd in current_place.connections:
            current_place = name_place[current_place.connections[cmd]]
            push_prev(current_place)    # save current place in "previous" list
            return True
    
        return False

## The "look" command

With the implementation of the *previous_places* list and sometimes
printing the short description of a Place when we move to it, it might be
nice to allow the player to see the long description of a Place since
that often mentions directions to move that may not be in the short
description.  We add a "look" command to the allowed commands.  This 
command just forces the *describe_place()* function to print the long
description of the current Place.

## Automatic generation of the name_place dictionary

We mentioned this change in the previous page, but now it's time to implement
the automatic generation of the *name_place* dictionary.  We want this for two
reasons:

* it is no longer necessary for the implementor to maintain the dictionary and
  perhaps make mistakes, and
* we will need this when we load map data from disk.

We **can** automatically generate the dictionary since everything we need is in
each instance of a Place object: we know the object reference and can get the
instance *.name* attribute.  We just have to examine every object declared at
the *globals()* level and process it if it is an instance of a Place.  We also
take the time to check that **all** the unique identifying name strings actually
are unique and print an error message as helpful as possible if not.  This helps
when there are very many Place instances:

    name_place = {}
    for (obj_name, obj) in globals().copy().items():    # MUST USE copy()!
        if isinstance(obj, Place):
            name = obj.name
            if name in name_place:      # check unique name is unique
                msg = f"Place in variable '{obj_name}' doesn't have a unique identifier: '{name}'"
                raise ValueError(msg)
            name_place[name] = obj

## Testing

We test the code, looking for the short and long descriptions:

    $ python3 TextAdventure.01.py 
    You are at the White house. Paths lead south and east from here.
    What do you want to do? e
    You are on a narrow path.
    What do you want to do? w
    You are at the White house.                     # short description
    What do you want to do? look                    # try 'look'
    You are at the White house. Paths lead south and east from here.
    What do you want to do? quit                    # QUIT works
    So long.

That's all for this page.  
The
[code is here](TextAdventure.01.py)
and
[on the next page](TextAdventure.02.md)
we add objects that the player may pick up and drop.
