# Text Adventure

The aim is to start with a very small text adventure and then add
features to get closer to the "professional" text adventures, like
[Advent](https://en.wikipedia.org/wiki/Colossal_Cave_Adventure) or
[Zork](https://en.wikipedia.org/wiki/Zork).

The usual approach adopted by most people is to start writing a function for
each place in the map.  The code in one function will call the code in the next
function to handle a player move in the map.  That doesn't seem the right way to
go as it's hard to handle objects that the user can pick up, move and then put
down, not to mention handling monsters that can move by themselves!  In addition,
that means the game is implemented recursively so there's a limit of about 1000
moves before the code crashes - not good.

Overall, it's best to separate the data describing the map, objects and monsters
from the code actually running the game.  In addition, the way to having one
body of code that can play many games becomes obvious - we just need different
data and we have a different game.

## Data Structures

So we will have data structures describing various things in the game.  What
sort of things will we have?  Well, we will have at least a player object
and a set of objects describing the places in the game.  The player object is
minimal at first - there isn't much we need to remember about the player, at
first, anyway.

It's different with the places in the map.  We need a description of the place,
something we print out to show the user.  We also need some way of describing 
how each place is connected to the other places in the map.  This raises a
problem.  Suppose we have a map with two places, *A* and *B*.  *A* connects to
*B* and *B* connects to *A*.  How do we define the connections?  When we define
the data for place *A* we need to somehow link it to place *B* which we haven't
defined yet?  Defining *B* is no problem since *A* is defined now, so we can
just use a reference to the *A* object as our connection.  But defining *A* to
connect to *B* is a problem, often called a "forward reference" problem.

There are two ways out of this dilemma.  The first is to **not** include
connections to other places when defining a place.  We can construct a
*connections* graph after we have defined the places.  That data structure
would probably be a dictionary that linked a reference to each place to a list
of places that can be moved to plus the direction we take to get there.  This,
however, isn't easy to use later when we want to write our game data structures
to a disk file.

The second way is to link the places together with a unique textual string that
describes each place.  That unique name would be an attribute of each place
and would also be used to link one place to another at the cost of a lookup.
We need to create a dictionary that joins the unique name to a reference to the
place object that holds that name.  This is the approach we use here.

## Dictionaries vs Objects

We *could* use dictionaries to hold all that information, but later on we want
to have code associated with game objects like monsters and we can't really
do that easily with dictionaries.  So we will use OOP objects instead.  If
you don't understand python OOP this might be a good place to learn as the
text adventure will only use simple OOP concepts.

## The Place Object

Each place in the map will be an instance of the *Place()* object, which has
this form:

    class Place:
        """A Place in the text adventure."""

        def __init__(self, name, description, connections):
            self.name = name
            self.description = description
            self.connections = connections

        def __str__(self):
            """For debug."""

            return f"Place('{self.name}')"

We see that the Place object has only three attributes and no real code.  As
mentioned before, the *self.name* attribute is the unique identifying string
for the Place and the *self.description* attribute is a string describing the
Place that will be shown to the user.

The *self.connections* attribute is more tricky.  It's a dictionary of keys
that are the directions the player can move from this Place.  The value for each
direction key is the unique identifying string of the Place that direction
connects to.

If we have this simple map showing the Places and directions between them:

    +-------------+ E  W +-------------+ E  W +-------------+`
    | white_house |------|    path     |------|    glade    |
    +-------------+      +-------------+      +-------------+
           |S                                     /SW
           |                                     /
           |N                                   /
    +-------------+ NW                         /
    |    forest   |--------------------------- 
    +-------------+

then we would start describing the Places in code as:

    white_house = Place('white_house', 'at the White house.',
                        connections={'east': 'path',
                                     'south': 'forest'})

    path = Place('path', 'on a narrow east-west path.',
                 connections={'east': 'glade',
                              'west': 'white_house'})

You see that the connections for the "white_house" are in a dictionary with
keys of "east" and "south" with the values for each direction being the 
identifying string of the Place to the east and south, respectively.

## The mapping of Place name to Place object

The Place objects contain everything we need to describe a game map, but we need
a method of connecting the unique string names in the *Place.connections*
dictionary to the actual Place object with that particular *Place.name*.
We create a dictionary for this:

    name_place = {'white_house': white_house,
                  'path': path,
                  'glade': glade,
                  'forest': forest
                 }

Suppose the player is at the White house and tries to move in the "east"
direction.  The Place object for the White house has this *connections*
attribute:

    connections={'east': 'path', 'south': 'forest'}

so we can plug "east" into the connections directory and get the destination
"path".  Using the *name_place* dictionary above we can get a reference to 
the destination Place with the name "path".

As we mentioned above, the *name_place* dictionary solves our "forward
reference" problem because we create the dictionary after all the Places have
been defined.

There is another reason why we want this dictionary, though.  We eventually want
to be able to load Place data from a disk file and we can't write python
references to a disk file and have them mean anything when we read them back
from disk.  If our Place data doesn't contain any actual Place references
then there is no problem saving and restoring Place data to/from disk.

But what about the *name_place* dictionary?  We can't write that to disk
because it contains Place object references, true, but we don't have to because
the *name_place* dictionary can be created automatically after creating all
the Place objects.  For now, however, we create the dictionary by hand.

## Canonical command strings

A well-written adventure allows a lot of flexibility in the way the user
types commands.  For instance, if the player can move towards the east
in a place, then the player could type in "east" or "e".

This flexibilty influences what we put into the *Place.connections* dictionary
as the command that moves to a Place.  We could place all allowed move commands
into the connections dictionary, but then we would need to change the Place()
call defining the White house from this:

    white_house = Place('white_house', 'at the White house.',
                        connections={'east': 'path',
                                     'south': 'forest'})

to this:

    white_house = Place('white_house', 'at the White house.',
                        connections={'east': 'path',
                                     'e': 'path,
                                     'south': 'forest',
                                     's': 'forest'})

which is a little verbose and only gets worse if there are more than two forms
of allowed commands.  Plus, someone has to create the data for each Place and
it would be nice if there isn't all that redundancy in static data.

The solution is to allow the user to enter a command in its full form or any 
"alias" allowed, but to convert the command to its "canonical" form before
returning the command string to the code handling a user command.

We will obviously write a function that takes user input and returns the 
canonical form.  That function uses a data structure describing all allowed
commands and their canonical form:

    allowed_moves = {'north': 'north', 'n': 'north',
                     'northeast': 'northeast', 'ne': 'northeast',
                     'east': 'east', 'e': 'east',
                     'southeast': 'southeast', 'se': 'southeast',
                     'south': 'south', 's': 'south',
                     'southwest': 'southwest', 'sw': 'southwest',
                     'west': 'west', 'w': 'west',
                     'northwest': 'northwest', 'nw': 'northwest',
                    }

The *allowed_moves* dictionary has keys that are all allowed move strings and
values that are the canonical form of that command.  This version of the
adventure only allows the player to move around the map so the dictionary
only has move commands in it.

# Game logic

Now that the data has been written it's time to write some code!

The basic loop at the heart of this adventure is:

    while True:
        describe_place(current_place)
        move = get_command()
        if not do_command(move):
            print("Sorry, you can't move in that direction.  Try again.\n")

The first thing to notice is that there is a global variable *current_place*
which holds a reference to the Place the player is at.  The first thing we do
in the loop is describe the place the player is at.

Once the Place is described, we ask the user for input.  The *get_command()*
function gets input from the player and returns the canonical command string.
The function does not check if a command is legal at the current Place, it
just returns the canonical form of any allowed command.

Then the function *do_command()* is called passing the *move* command string.
This function returns *True* if the move is legal at the current Place and
*False* if it is not.  We print an error message if the command was not legal.

And that's about all there is to our first text adventure!

The
[code is here](TextAdventure.00.py)
and we run a small test on it:

    $ python3 TextAdventure.00.py
    You are at the White house.
    Which way? east
    You are on a narrow path.
    Which way? n   
    Sorry, you can't move in that direction.  Try again.
    
    You are on a narrow path.
    Which way? e
    You are in a shadowed glade.

[On the next page](TextAdventure.01.md)
we add some small and not so small features.
