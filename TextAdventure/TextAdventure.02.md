# Player pick up

We want to allow the player to pick up and drop objects in the map.  First we
have to add objects to the map.  We can't just describe objects at a Place in
the Place description because that would make them static - the player couldn't
pick them up and move them.  So we need an Object class and we have to change
the Place class to contain the Objects that are at that Place.

## Objects

First we define an Object class.  It's very similar to the Place class:

    class Object:
        """An object."""
    
        def __init__(self, name, description, place, long_description=None):
            self.name = name
            self.description = description
            self.place = place
            self.long_description = description
            if long_description:
                self.long_description = long_description
    
        def __str__(self):
            """For debug."""
    
            return f"Object('{self.name}')"

Note that an Object has a *.place* attribute that is the unique string
referencing the Place that this Object is at.  We will use this information
later to place Objects into their initial Place in the game.

We also have to alter the Place class to have somewhere to store the objects
that are at the Place:

    class Place:
        """A Place in the text adventure."""
    
        def __init__(self, name, description, connections, long_description=None):
            self.name = name
            self.description = description
            self.connections = connections
            self.long_description = description
            if long_description:
                self.long_description = long_description
            self.objects = []           # a place to hold Object names

In the same way we reference other Places from one Place, the *.objects*
attribute is just a list holding the unique name strings of zero or more
Objects.

So we can now define the Objects in our adventure:

    # the objects in this adventure
    axe = Object('axe', 'a small Elvish axe.', 'glade',
                 long_description='a small Elvish axe.  '
                                  'There are faint unreadable engravings on the head.')

We also need to run a bit of code to place the Objects in their initial Place.
We also define a dictionary *object_name_ref* that maps an object name string to
a reference to the actual Object instance:

    # code to place all objects in their initial position in the map
    # we also need to populate the "object_name_ref" ditionary
    object_name_ref = {}
    for (obj_name, obj) in globals().copy().items():
        if isinstance(obj, Object):
            name = obj.name
            if name in object_name_ref:     # check unique name is unique
                msg = f"Object in variable '{obj_name}' doesn't have a unique identifier: '{name}'"
                raise ValueError(msg)
            object_name_ref[name] = obj
    
            # place Object into the required Place
            place = obj.place
            place_ref = globals()[place]
            place_ref.objects.append(name)

## Place description lists Objects

All the code above places Objects into the map.  Now we have to modify the 
code that prints a Place description to also show any Objects that are present:

    def describe_place(place, look=False):
        if look or place not in previous_places[1:]:
            print(f"You are {place.long_description}")
        else:
            print(f"You are {place.description}")
    
        # if there's an Object here, print its description
        if place.objects:
            print('\nYou see here:')
            for obj_name in place.objects:
                print(f'\t{object_name_ref[obj_name].description}')

## Testing

Now that we've done all that, it's time to test.  We should see an Elvish axe
in the glade:

    $ python3 TextAdventure.02.py 
    You are at the White house. Paths lead south and east from here.
    What do you want to do? e
    You are on a narrow path.
    What do you want to do? e
    You are in a shadowed glade, with paths to the west and southwest.
    
    You see here:
    	a small Elvish axe.
    What do you want to do? sw
    You are in a dark difficult forest. Narrow tracks go northeast and north.

It works!

The
[code is here](TextAdventure.02.py).
Try adding an object of your own.


[On the next page](TextAdventure.03.md)
we allow the player to pick up and drop objects.
