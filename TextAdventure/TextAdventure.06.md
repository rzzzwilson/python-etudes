# Restoring from a file

Now we need to restore from a save file, if there is one.

## Restoring

The top-level code to handle either restoring from a save file or initializing
the game to the original state is:

    if os.path.exists('state.json') and os.path.isfile('state.json'):
        if not restore_state('state.json'):
            init_default_state()
    else:
        init_default_state()

Here we call the function `restore_state('state.json')` to restore the game
from the save file.  If that *doesn't* work, or there is no save file, we
initialize the game to its start state with the `init_default_state()` function.

## Restoring from the save file

This isn't conceptually hard.  We just reverse the process that wrote the
save file in the first part.  The fiddly part is creating all the objects
in the global space because we don't know the original names in global space.
The code is:

    def restore_state(fname):
        """Restore game state from the given file.
    
        If all went OK, return True, else False (ie, error reading file).
        """
    
        with open(fname, 'r') as fd:
            restore_data = fd.read()
    
        try:
            restore_dict = json.loads(restore_data)
        except json.decoder.JSONDecodeError:
            # bad state file!?
            return False
    
        # restore data from the file
        for place in restore_dict['Place']:
            name = place['name']
            globals()[name] = Place(**place)
    
        for obj in restore_dict['Object']:
            name = obj['name']
            globals()[name] = Object(**obj)
    
        for monster in restore_dict['Monster']:
            name = monster['name']
            globals()[name] = Monster(**monster)
    
        global player
        player = Player(**restore_dict['Player'])
    
        map_instances_ref()
    
        global current_place
        current_place = place_name_ref[restore_dict['current_place']]
    
        global allowed_commands
        allowed_commands = restore_dict['allowed_commands']
    
        return True

After opening the JSON save file and decoding the contents we just iterate
through the `Place`, `Object`, `Monster` lists and create instances of the
objects in the global namespace.  We do that by accessing the global namespace
through the dictionary returned by the `globals()` builtin.  We create the
object instance and put it into the global dictionary with the given name from
the save data.

The `Player` object is handled in a slightly different way since we know the
name of the variable holding a reference to the Player object.  We just create
it globally.

After all that we do the usual "name to reference" mappings.

Finally, we create the "current_place" and "allowed_commands" globals.  We don't
need to fiddle with the `globals()` dictionary to do this since we know their
names, and we just create them globally as we did for "player".

This is all a bit fiddly and uses some python "magic" to get the data defined
the right way in the right place, but it's not too advanced and the fiddling
is nicely contained in a smallish function.

## Initializing game data

If there is no save file to restore from we want to initialize the
game data as we have been doing up to this point.  We do that in the
`init_default_state()` function.  This is pretty much the same as the
process of restoring from file except that we know the names of all objects
and we don't have to put things into the `globals()` dictionary, we can
just do it with a `global` statement followed by an assign.

This should be done by importing a module and calling the initialization
function within the module because the initial data will be very large and
different games will have have different initialization modules.  It just makes
sense to separate the game "engine" code and the game-specific data.  But for
the purposes of this etude we don't do that.

## Testing

Testing is just a matter of playing the game to get into particular
circumstances, such as the player holding the axe, a monster in the same place
as the player, the player being in a place with some object, etc.  Quit the game
and look at the save JSON file, as before.  That should work since we tested it
before.

The extra step now is to execute the game code and see the restore of game data
from the save file.  The state of the game should match what we saw in the JSON
save file.

The
[code is here](TextAdventure.06.py)
and you should test it yourself.

## Finally

That's as far as we go in this etude.  There are many enhancements you can make
to the "engine" code, such as:

* allow combat between player and a monster
* create "health points" for the player and moster objects
* allow conversations between the player and monster objects
* write a "level editor" that will allow edits to the game data structures
* etc
