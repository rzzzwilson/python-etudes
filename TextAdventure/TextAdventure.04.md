# Adding Monsters

So far we have a very boring map.  True, we can pick things up and carry them
around the map and drop them wherever we like.  But there're no "monsters" - 
living creatures we can interact with.  We will add them now.

The attributes of monsters that interest us the most are:

* Monsters can move around the map
* Monsters can attack the player, and can kill the player
* The player can kill a Monster

For a start, we just want a monster that can move around the map.  We will
create additional monsters by inheriting from the *Monster* class.

## The Monster Object

Apart from the "move" behaviour, Monsters are rather like inanimate objects,
though that will change later!  So we start with the Object class for a
beginning.  The "move" behaviour we can implement by each monster having a
*move()* method which moves each Monster.  Different move behaviours will
require each Monster subclass to be derived from the base Monster class and
override the `move()` method.

The main loop code will call each Monster's *move()* method for each move.
So the Monster class looks like:

    class Monster:
        """A monster object."""
    
        def __init__(self, name, description, place):
            self.name = name
            self.description = description
            self.place = place
    
        def move(self):
            """The monster moves in a random direction about 50% of the time."""
    
            # decide if monster moves
            if random.random() < 0.5:
                return      # no move this turn
    
            # look at the current "place" for possible moves, pick one
            place_ref = place_name_ref[self.place]
            direction = random.choice(list(place_ref.connections.keys()))
    
            # figure out where that leads to and move
            new_place = place_ref.connections[direction]
            new_place_ref = place_name_ref[new_place]
            place_ref.monsters.remove(self.name)
            new_place_ref.monsters.append(self.name)
    
            # finally, update self.place since that will be used when loading from file
            self.place = new_place
    
        def __str__(self):
            """For debug."""
    
            return f"Monster('{self.name}')"

As before, the `self.name` attribute is an identifying string that uniquely
identifies each Monster instance, and the `self.description` is the monster's
description.  The `self.place` attribute holds the string identifying the
place the Monster is at.

The `move()` code is quite simple.  About 50% of the time the monster won't
move.  When it does move, it chooses randomly between all the connections for
the Place the Monster is at.

## Creating a monster

We create a monster in the same way we create objects like the axe:

    # the monster in this adventure
    goblin = Monster('goblin', 'A hairy goblin with very bad breath.', 'glade')

## Placing Monsters at start

We place the Monster instances at their initial start place in much the same way
we place inanimate Object instances.  We loop through all declared python objects
and decide which are Monster instances.  Then we place the Monster instance in
the correct Place.  We use the same loop that we handle placing Objects:

    object_name_ref = {}
    monster_name_ref = {}
    for (obj_name, obj) in globals().copy().items():
        if isinstance(obj, Object):
            # object code, as before
        elif isinstance(obj, Monster):
            name = obj.name
            if name in monster_name_ref:    # check unique name _is_ unique
                msg = f"Monster in variable '{obj_name}' doesn't have a unique identifier: '{name}'"
                raise ValueError(msg)
            monster_name_ref[name] = obj
    
            # place Monster into the required Place
            place = obj.place
            place_ref = place_name_ref[place]
            place_ref.monsters.append(name)

## Testing

This is slightly tricky because the goblin can move, but if we just keep moving
around the map we should catch up with it.  Once we find the goblin we just wait
at that spot and we should see the goblin disappear as it moves away:

    $ python3 TextAdventure.04.py 
    You are at the White house. Paths lead south and east from here.
    What do you want to do? e
    
    You are on a narrow east-west path.
    What do you want to do? e
    
    You are in a shadowed glade, with paths to the west and southwest.
    
    You see here:
    	a small Elvish axe.
    What do you want to do? sw
    
    You are in a dark difficult forest. Narrow tracks go northeast and north.
    
    You see:
    	A hairy goblin with very bad breath.
    What do you want to do? wait
    Time passes...
    
    You are in a dark difficult forest. Narrow tracks go northeast and north.

The
[code is here](TextAdventure.04.py)
and you should test it yourself.

[On the next page](TextAdventure.05.md)
we write game data to disk.
