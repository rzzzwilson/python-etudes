# A level editor

So far the initial game data exists either as code or in a save file.  That's a
problem when we start adding lots of new places, objects and monsters, since we 
have to keep track of text links, etc.  It would be better if we can use a
"level editor" that helps with the task.

A level editor behaves much like the game playing code except:

* No time-sensitive actions occur.  For example, monsters never move.
* There are extra commands to add or delete objects, create connections
  to new rooms etc, edit existing place or object descriptions, and so on.

The level editor could be a separate program but here we will build the editing
functionality into the existing game code.  This means we don't have to maintain
two versions of the code that allows a player to move around the map.  Of course,
we should "turn off" the editing functions until editing is required.

Here is a transcript of how the level editor might be used:

    $ python3 Textadventure.07.py
    You are at the White house. Paths lead south and east from here.
    What do you want to do? EDIT

    Edit mode enabled...
    EDIT: You are at the White house. Paths lead south and east from here.
    EDIT: What do you want to do? EAST
    EDIT:
    EDIT: You are on a narrow east-west path.
    EDIT: What do you want to do? EDIT PLACE
    EDIT: 
    EDIT: This place has attributes:        # SHOULD ONLY LIST CHANGEABLE ATTRIBUTES
    EDIT:     name              'path'
    EDIT:     description       'You are on a narrow east-west path.'
    EDIT:     connections       'east', 'west'
    EDIT:     long_description  None
    EDIT:     objects           []
    EDIT:     monsters          []
    EDIT: ? description=You are on a very narrow east-west path.
    EDIT: ? END
    EDIT: 
    EDIT: You are on a very narrow east-west path.
    EDIT: What do you want to do?

We used the `EDIT` command to go into edit mode.  From that point all output
lines are prefixed with "EDIT: " to remind us that we are in edit mode.  We
can move around as before.  We want to change the description string of the
current place (path), so we input `EDIT PLACE` to start the process.  The code
first dislays the existing attributes of the place and then waits for edit
commands.  


The
[code is here](TextAdventure.07.py)
and you should test it yourself.

## Finally

That's as far as we go in this etude.  There are many enhancements you can make
to the "engine" code, such as:

* allow combat between player and a monster
* create "health points" for the player and moster objects
* allow conversations between the player and monster objects
* etc
