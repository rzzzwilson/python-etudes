# inner functions are also enclosures

def test():
    def test2():
        print(f'test: {i=}')

    func_list = []
    for i in range(3):
        func_list.append(test2)

    # debug
    i = 42
    print(f'After creating the functions, change "i" to {i=}')

    return func_list

# create a list of lambda functions defined in the test() function
flist = test()

# show that "i" isn't defined in the local namespace
try:
    print(f'{i=}')
except NameError:
    print('Variable "i" is not defined in this namespace')

# call each lambda in turn
for func in flist:
    func()
