# use default parameters

# create a list of lambda functions that should print 0, 1, 2
lambda_list = []
for i in range(3):
    lambda_list.append(lambda x=i: print(f'lambda: {x=}'))

# call the functions in turn
for func in lambda_list:
    func()
