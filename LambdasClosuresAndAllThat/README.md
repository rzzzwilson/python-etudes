# The problem

Beginners, and others, often trip over this behaviour with lambdas
(code in *Lambdas_00.py*):

    # create a list of lambda functions that should print 0, 1, 2
    lambda_list = []
    for i in range(3):
        lambda_list.append(lambda: print(f'lambda: {i=}'))
    
    # call the functions in turn
    for func in lambda_list:
        func()

In this code the programmer expects the lambda function to print the value of `i`
that prevailed when the lambda was defined, but what is actually printed is:

    lambda: i=2
    lambda: i=2
    lambda: i=2

that is, the three lambda functions print the *final* version of the
variable `i`.  What is going on?

# Enclosures

In the example above the lambda code is evaluating a name `i` that exists
outside the lambda namespace, so the lambda is given a special namespace
containing the environment the lambda was defined in.  This namespace is
called an *enclosure*.  In this case the enclosure is just the program global
namespace.

To show how this works let us change the value of the `i` variable
after the lambda functions are created and also change the value after calling
the lambdas.  The code calls the lambdas after each change (code in *Lambdas_01.py*):

    # create a list of lambda functions that should print 0, 1, 2
    lambda_list = []
    for i in range(3):
        lambda_list.append(lambda: print(f'lambda: {i=}'))
        
    # debug
    print('Set "i" to 42 after creating the lambdas')
    i = 42
    
    # call the functions in turn
    for func in lambda_list:
        func()
        
    # more debug, set "i" to another value
    print('Set "i" to "abc" before calling the lambdas again')
    i = "abc"
    
    # call the functions in turn again
    for func in lambda_list:
        func()

This prints:

    Set "i" to 42 after creating the lambdas
    lambda: i=42
    lambda: i=42
    lambda: i=42
    Set "i" to "abc" before calling the lambdas again
    lambda: i='abc'
    lambda: i='abc'
    lambda: i='abc'

Now it's clear that the lambda function is printing the value of `i` at the
point where the lambda is executed.

For more on closures, see
[the Wikipedia page](https://simple.wikipedia.org/wiki/Closure_\(computer_science\)).

# Another problem

Let's try a more complicated example where the closure environment **isn't** the global
namespace.  We define some lambdas inside a function and return the list of lambdas
(code in *Lambdas_02.py*):
    
    def test():
        lambda_list = []
        for i in range(3):
            lambda_list.append(lambda: print(f'lambda: {i=}'))
        return lambda_list
    
    # create a list of lambda functions defined in the test() function
    llist = test()
    
    # show that "i" isn't defined in the local namespace
    try:
        print(f'{i=}')
    except NameError:
        print('Variable "i" is not defined in this namespace')
    
    # call each lambda in turn
    for func in llist:
        func()

When we execute this code we see:

    Variable "i" is not defined in this namespace
    lambda: i=2
    lambda: i=2
    lambda: i=2

Note that even though `i` isn't defined in the local namespace the lambda
function still prints a value for `i`.  That's because `i` is defined in the 
namespace (or environment) that is associated with the lambda function, the
*closure*.  Just what namespace is it that is associated with the lambda?
It's probably the function namespace.  We can test that by adding some debug
code that changes the value of `i` after the lambdas are created in the
function (code in *Lambdas_03.py*):

    def test():
        lambda_list = []
        for i in range(3):
            lambda_list.append(lambda: print(f'lambda: {i=}'))
    
        # debug
        i = 42
        print(f'After creating the lambdas, change "i" to {i=}')
    
        return lambda_list
    
    # create a list of lambda functions defined in the test() function
    llist = test()
    
    # show that "i" isn't defined in the local namespace
    try:
        print(f'{i=}')
    except NameError:
        print('Variable "i" is not defined in this namespace')
    
    # call each lambda in turn
    for func in llist:
        func() 

When executed, this is printed:

    After creating the lambdas, change "i" to i=42
    Variable "i" is not defined in this namespace
    lambda: i=42
    lambda: i=42
    lambda: i=42

Now it's very clear that the enclosure we created is the environment inside the
function that we created the lambda function in.  Normally when we exit
a function the namespace inside the function is destroyed because, like any
other object in python, we decrease the reference count of the namespace
which sets the reference count to 0 and the function namespace is garbage
collected.  However, when we create a closure that enclosure also references
the function namespace so the namespace isn't deleted when the function is 
exited.

# It's not just lambdas

At the moment you might think that this enclosure weirdness is just something
to do with lambdas.  But that isn't so, it exists for any executable code.
Here's an example using local functions rather than lambdas (code in *Lambdas_04.py*):

    def test():
        def test2():
            print(f'test: {i=}')
    
        func_list = []
        for i in range(3):
            func_list.append(test2)
    
        # debug
        i = 42
        print(f'After creating the functions, change "i" to {i=}')
    
        return func_list
    
    # create a list of lambda functions defined in the test() function
    flist = test()
    
    # show that "i" isn't defined in the local namespace
    try:
        print(f'{i=}')
    except NameError:
        print('Variable "i" is not defined in this namespace')
    
    # call each lambda in turn
    for func in flist:
        func()

When executed:

    After creating the functions, change "i" to i=42
    Variable "i" is not defined in this namespace
    test: i=42
    test: i=42
    test: i=42

just like the same code using lambdas.

# Solving the problem

So how do we solve the problem?  We would like the lambdas (and the similar
functions) to print the value of `i` that existed when we defined the lambda.
The problem is that the code we've seen so far only evaluates its variables
when the code is executed.

One way to solve this is to give the lambda definition a 
standard *default parameter*, like this (code in *Lambdas_05.py*):

    # create a list of lambda functions that should print 0, 1, 2
    lambda_list = []
    for i in range(3):
        lambda_list.append(lambda x=i: print(f'lambda: {x=}'))          # "x" gets default value "i"
    
    # call the functions in turn
    for func in lambda_list:
        func()

Default parameter values are created at function/lambda *definition time*
and are stored in the function/lambda object.  So we see this when executed:

    lambda: x=0
    lambda: x=1
    lambda: x=2

which is exactly what we want.

A similar solution for the "function" case (code in *Lambdas_06.py*):

    def test():
        func_list = []
        for i in range(3):
            def test2(x=i):
                print(f'test: {x=}')
    
            func_list.append(test2)
    
        return func_list
    
    # get and call the functions in turn
    flist = test()
    for func in flist:
        func()

Note that we must define the inner function where `i` is defined, that is,
inside the loop varying `i`.  When run we see this, as expected:

    test: x=0
    test: x=1
    test: x=2

# Using functools.partial()

You can also use the
[*functools.partial()*](https://docs.python.org/3/library/functools.html#functools.partial)
function to *freeze* a number of arguments to a function.

Here's a short example of using *partial()* (code in *Lambdas_07.py*):

    # example of functool.partial() usage

    from functools import partial

    def sum_all(*args):
        return sum(args)

    data = list(range(5))
    print(f'{data=}')
    print(f'{sum_all(*data)=}')

    sum_all_plus_10 = partial(sum_all, 10)
    print(f'{data=}')
    print(f'{sum_all_plus_10(*data)=}')

We define a function *sum_all()* that sums all of its numeric parameters.
We can pass any number of arguments and the function will return the sum
of those values.

In this line, however:

    sum_all_plus_10 = partial(sum_all, 10)

we define another function *sum_all_plus_10()* that is the *sum_all()*
function with a single frozen argument 10.  When we call the new function
passing the list *data* we get the sum of *data* plus the frozen parameter
10, like this:

    data=[0, 1, 2, 3, 4]
    sum_all(*data)=10
    data=[0, 1, 2, 3, 4]
    sum_all_plus_10(*data)=20

We can use this to solve the lambda evaluation problem (code in *Lambdas_08.py*):

    # using functools.partial()

    from functools import partial

    # create a list of lambda functions that should print 0, 1, 2
    lambda_list = []
    for i in range(3):
        # append the function returned by "partial()"
        lambda_list.append(partial(lambda x: print(f'lambda: {x=}'), i))

    # call the functions in turn
    for func in lambda_list:
        func()

When run, this code prints:

    lambda: x=0
    lambda: x=1
    lambda: x=2

A similar approach will solve the function closure problem.

# Wrap up

You have been shown two ways around the problem of lambdas not accessing the
desired value.  Of the two I prefer the "default parameter" method.  The
*functools.partial()* method is more general, but the default parameter
method is probably easier for a beginner to understand in these simple
cases.
