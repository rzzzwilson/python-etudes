# using functools.partial()

from functools import partial

# create a list of lambda functions that should print 0, 1, 2
lambda_list = []
for i in range(3):
    # append the function returned by "partial()"
    lambda_list.append(partial(lambda x: print(f'lambda: {x=}'), i))

# call the functions in turn
for func in lambda_list:
    func()

