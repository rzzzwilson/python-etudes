# lambdas defined inside a function

def test():
    lambda_list = []
    for i in range(3):
        lambda_list.append(lambda: print(f'lambda: {i=}'))
    return lambda_list

# create a list of lambda functions defined in the test() function
llist = test()

# show that "i" isn't defined in the local namespace
try:
    print(f'{i=}')
except NameError:
    print('Variable "i" is not defined in this namespace')

# call each lambda in turn
for func in llist:
    func()
