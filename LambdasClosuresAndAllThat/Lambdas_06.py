# inner functions are also enclosures

def test():
    func_list = []
    for i in range(3):
        def test2(x=i):
            print(f'test: {x=}')

        func_list.append(test2)

    return func_list

# get and call the functions in turn
flist = test()
for func in flist:
    func()
