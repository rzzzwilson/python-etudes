# after ading some debug code

# create a list of lambda functions that should print 0, 1, 2
lambda_list = []
for i in range(3):
    lambda_list.append(lambda: print(f'lambda: {i=}'))

# debug
print('Set "i" to 42 after creating the lambdas')
i = 42

# call the functions in turn
for func in lambda_list:
    func()

# more debug, set "i" to another value
print('Set "i" to "abc" before calling the lambdas again')
i = "abc"

# call the functions in turn again
for func in lambda_list:
    func()
