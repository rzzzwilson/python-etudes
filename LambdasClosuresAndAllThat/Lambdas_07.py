# example of functool.partial() usage

from functools import partial

def sum_all(*args):
    return sum(args)

data = list(range(5))
print(f'{data=}')
print(f'{sum_all(*data)=}')

sum_all_plus_10 = partial(sum_all, 10)
print(f'{data=}')
print(f'{sum_all_plus_10(*data)=}')
