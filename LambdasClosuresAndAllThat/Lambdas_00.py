# program demonstrating the problem
# the coder wants to create a set of lambda functions containing a unique number
# but all the lambdas print the same final number: 2

# create a list of lambda functions that should print 0, 1, 2
lambda_list = []
for i in range(3):
    lambda_list.append(lambda: print(f'lambda: {i=}'))

# call the functions in turn
for func in lambda_list:
    func()
