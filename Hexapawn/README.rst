Hexapawn
========

This etude was prompted by a reddit /r/learnpython question:

    https://old.reddit.com/r/learnpython/comments/tja1us/how_would_i_start_and_do_this/

I pointed the poster to an article written in Scientific American many years
ago in March, 1962:

    http://www.cs.williams.edu/~freund/cs136-073/GardnerHexapawn.pdf

I also mentioned a more recent article here:

    https://computd.nl/demystification/hexapawn-accessible-machine-learning/

Both of the above articles are available in this directory.

I suggested to the reddit poster that this would be a good project to start
simply in Artificial Intelligence.  While the approach used in the "matchbox"
implementation is simple and can only be used when the complete game tree can
be stored, it is a form of machine learning.

Python Implementation
=====================

A python implementation raises a few questions, some trivial:

* How do we recognize a player move?
* What do we use instead of matchboxes?
* How do we recognize a particular position?
* How do we reinitialize the learning, ie, make the game forget everything learned?

In addition, do we write a text-only game or do we try for a GUI version?  In
the (slightly modified) words of Tony Stark, "can't we have both?".

Recognize a player move
-----------------------

We could use almost anything, and probably will in the beginning, but we 
eventually want something like a simplified chess notation.

Matchboxes?
-----------

The contents of the matchbox will be a list.  We can randomly choose and remove
an item from a list.

Recognize a position?
---------------------

The matchbox version relies on the player to recognize the board position
on a matchbox.  We could somehow characterize a board position as a string
of characters, such as "B" for a black pawn, "W" for a while pawn and " "
for an empty square.  This string could be used as a key into a dictionary
where each value is a list of the allowed computer moves in that position.

Reinitialize
------------

This is simple.  We create a dictionary as described above containing all the
allowed moves as shown in the article(s).  When we start the game the first
time we would deepcopy this initial dictionary into a game dictionary that is
modified during the learning process.

Reinitialization is easy - we just repeate the deepcopy to recreate an
initialized game dictionary.
