"""
An implementation of Hexapawn.  Version x0.

The positions drawn on the matchboxes are claimed to not include the mirror
image position and the user has to match the board position with the possibly
mirrored position on the matchboxes.  However, some of the 24 positions in the
original article in 1962 are obviously mirrors of other positions in the 24.
The same data is in the more recent article, which appears to be copied from
the 1962 article.

The code here implements the "position" string reverse function and a move
reversal function.  These will be needed in the final game but can also be
used to identify the unexpected mirrors in the published data.
"""

from Hexapawn_00 import MoveTree

def position_reverse(position):
    return position[:3][::-1] + position[3:6][::-1] + position[6:][::-1]

xmirror = {0: 2, 1: 1, 2: 0}

def move_reverse(move):
    (fromx, fromy, tox, toy) = move
    return (xmirror[fromx], fromy, xmirror[tox], toy)

def moves_reversed(moves):
    return [move_reverse(m) for m in moves]


def equal_moves(a, b):
    return sorted(a) == sorted(b)

#position = "BBB   WWW"
#mirror = position_reverse(position)
#print(f'{position=}, {mirror=}')
#
#position = "BB B WWW "
#mirror = position_reverse(position)
#print(f'{position=}, {mirror=}')
#
#move = (0, 2, 1, 1)
#mirror = move_reverse(move)
#print(f'{move=}, {mirror=}')
#
#move = (0, 2, 0, 1)
#mirror = move_reverse(move)
#print(f'{move=}, {mirror=}')
#
#move = (2, 1, 1, 0)
#mirror = move_reverse(move)
#print(f'{move=}, {mirror=}')

# now step through state keys, mirror the key and see if there is a mirrored key.
# if so, check that the moves in original are the mirrored moves in the mirror position.
count = 0
for (position, moves) in MoveTree.items():
#    print(f'{position=}, {moves=}')
    reversed_position = position_reverse(position)
    if reversed_position != position and reversed_position in MoveTree:
        count += 1
        print(f'Mirror {count:2d}: {position=}, {reversed_position=}, ', end='')
        mirror_moves = MoveTree[reversed_position]
        reversed_moves = moves_reversed(mirror_moves)
        if equal_moves(reversed_moves, moves):
            print('Moves are same')
        else:
            print('Moves NOT same')
