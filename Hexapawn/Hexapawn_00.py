"""
An implementation of Hexapawn.  Version 00.

Here we just define the data structures used to play the game.
Some simple code to check that the copy of game state is deep.

Since the game data may change, this module is designed to be 
imported to get access to "MoveTree".
"""


# The initial game state with all moves for each position
# The dictionary keys are position characterizations and
# the list value contains all the possible moves
# moves are (x0, y0, x1, y1) indicating a move from 1 to 2
MoveTree = {'BBBW   WW': [(1, 0, 0, 1), (1, 0, 1, 1), (2, 0, 2, 1)],                # 0
            'BBB W W W': [(0, 0, 0, 1), (0, 0, 1, 1)],                              # 1
            'B BBW   W': [(0, 0, 1, 1), (2, 0, 1, 1), (2, 0, 2, 1), (0, 1, 0, 2)],  # 2
            ' BBWB   W': [(1, 0, 0, 1), (2, 0, 2, 1), (1, 1, 1, 2)],                # 3
            'B BWW  W ': [(0, 0, 1, 1), (2, 0, 1, 1), (2, 0, 2, 1)],                # 4
            'BB W W  W': [(1, 0, 0, 1), (1, 0, 1, 1), (1, 0, 2, 1)],                # 5

            ' BB BWW  ': [(1, 0, 2, 1), (1, 1, 0, 2), (1, 1, 1, 2)],                # 6
            ' BBBWWW  ': [(1, 0, 2, 1), (2, 0, 1, 1)],                              # 7
            'B BB W W ': [(0, 1, 0, 2), (0, 1, 1, 2)],                              # 8
#            'BB WWB  W': [(0, 0, 1, 1), (1, 0, 0, 1)],                              # 9     mirror of # 7
            ' BB W   W': [(2, 0, 1, 1), (2, 0, 2, 1)],                              # 10
            ' BB W W  ': [(2, 0, 1, 1), (2, 0, 2, 1)],                              # 11

            'B BW    W': [(2, 0, 2, 1)],                                            # 12
            '  BBBW   ': [(0, 1, 0, 2), (1, 1, 1, 2)],                              # 13
            'B  WWW   ': [(0, 0, 1, 1)],                                            # 14
            ' B BWW   ': [(1, 0, 2, 1), (0, 1, 0, 2)],                              # 15
#            ' B WWB   ': [(1, 0, 0, 1), (2, 1, 2, 2)],                              # 16    mirror of # 15
            'B  BBW   ': [(0, 1, 0, 2), (1, 1, 1, 2)],                              # 17

#            '  BWBB   ': [(1, 1, 1, 2), (2, 1, 2, 2)],                              # 18    mirror of # 17
            '  BBW    ': [(2, 0, 1, 1), (2, 0, 2, 1), (0, 1, 0, 2)],                # 19
            ' B WB    ': [(1, 0, 0, 1), (1, 1, 1, 2)],                              # 20
#            ' B  BW   ': [(1, 0, 2, 1), (1, 1, 1, 2)],                              # 21    mirror of # 20
            'B  BW    ': [(0, 0, 1, 1), (0, 1, 0, 2)],                              # 22
#            '  B WB   ': [(2, 0, 1, 1), (2, 1, 2, 2)],                              # 23    mirror of # 22
           }


if __name__ == '__main__':
    from copy import deepcopy

    # This is the "working" copy of MoveTree that is modified during learning
    # Must be copied from MoveTree using a deep copy
    GameState = None
    
    # a bit of code to test deep copy
    GameState = deepcopy(MoveTree)
    del GameState['BBBW   WW'][1]
    print(f"{MoveTree['BBBW   WW']=}")
    print(f"{GameState['BBBW   WW']=}")
    
    
