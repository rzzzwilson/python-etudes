"""
An implementation of Hexapawn.  Version 01.

Make a start on the game main loop.
"""

import random
from copy import deepcopy
from Hexapawn_00 import MoveTree


# the initial board
InitialBoard = [['B', 'B', 'B'],
                [' ', ' ', ' '],
                ['W', 'W', 'W']
               ]

# This is the "working" copy of MoveTree that is modified during learning
# Must be copied from MoveTree using a deep copy
GameState = None

# the game board, modified from InitialBoard
GameBoard = None


def get_yes_no(prompt):
    print()
    while True:
        response = input(prompt).lower()[:1]
        if response in 'yn':
            return response
        print('Sorry, only YES or NO.')

def print_board(board):
    print()
    print('    0   1   2')
    print('  +---+---+---+')
    print(f'0 | {board[0][0]} | {board[0][1]} | {board[0][2]} |')
    print('  +---+---+---+')
    print(f'1 | {board[1][0]} | {board[1][1]} | {board[1][2]} |')
    print('  +---+---+---+')
    print(f'2 | {board[2][0]} | {board[2][1]} | {board[2][2]} |')
    print('  +---+---+---+')
    print()

def get_board_position(board):
    """Return the "position string" from the board."""

    position = []
    for y in range(3):
        for x in range(3):
            position.append(board[y][x])
    print(f'get_board_position: {position=}')
    return ''.join(position)

def get_player_move():
    while True:
        move_str = input('Player move: ')
        if not move_str:
            continue
        move = move_str.split()
        if len(move) != 2:
            print("Sorry, a move is something like '0,1 1,1'.")
            continue
        (start, end) = move
        start = start.split(',')
        end = end.split(',')

        if start[0] not in '012' or start[1] not in '012':
            print("Sorry, a move is something like '0,1 1,1'.")
            continue
        if end[0] not in '012' or end[1] not in '012':
            print("Sorry, a move is something like '0,1 1,1'.")
            continue

        return (int(start[0]), int(start[1]), int(end[0]), int(end[1]))

def legal_move(board, move):
    """Check if a player move is legal."""

    (startx, starty, endx, endy) = move
    print(f'{startx=}, {starty=}, {endx=}, {endy=}')

    # is the start position 'W'?
    if board[starty][startx] != 'W':
        return False

    # is the end position blank or 'B'?
    if board[endy][endx] not in ' B':
        return False

    # is the move one square up or diagonal
    if startx == endx:
        # UP move, only one square?
        if endy != starty - 1:
            return False
    elif (endx == startx + 1) or (endx == startx - 1):
        # DIAGONAL, only one square up?
        if endy != starty - 1:
            return False
    else:
        return False

    return True

def update_board(board, move):
    """Update board with a move."""

    (startx, starty, endx, endy) = move

    colour = board[starty][startx]
    board[starty][startx] = ' '
    board[endy][endx] = colour

def get_computer_move(position, state):
    print(f'get_computer_move: {position=}, {state=}')
    moves = state.get(position, None)
    if moves is None:
        return None

    return random.choice(moves)

def winner(board):
    print(f'winner: {(board[0][0], board[0][1], board[0][2])=}')
    if 'W' in (board[0][0], board[0][1], board[0][2]):
        return 'W'
    print(f'winner: {(board[2][0], board[2][1], board[2][2])=}')
    if 'B' in (board[2][0], board[2][1], board[2][2]):
        return 'W'
    return None

def play_one_game(board, state):
    print()
    print_board(board)

    while True:
        # get legal player move
        while True:
            pmove = get_player_move()
            print(f'{pmove=}')
            if legal_move(board, pmove):
                break
            print("Sorry, that isn't a legal W move.")

        # update board with player move
        update_board(board, pmove)
        print_board(board)

        # a win?
        if colour := winner(board):
            print(f'{colour} wins!')
            return colour

        # get computer move
        position = get_board_position(board)
        print(f'play_one_game: {position=}, {GameState=}')
        cmove = get_computer_move(position, state)
        print(f'play_one_game: {cmove=}')
        if cmove is None:
            # computer lost
            colour = 'W'
            print(f'{colour} wins!')
            return colour

        # update board with computer move
        update_board(board, cmove)
        print_board(board)

        # a win?
        if colour := winner(board):
            print(f'{colour} wins!')
            return colour

def main():
    # the game loop
    while True:
        GameState = deepcopy(MoveTree)
        GameBoard = deepcopy(InitialBoard)
        print(f'main: {GameState=}, {GameBoard =}')
    
        result = play_one_game(GameBoard, GameState)
        print(f'main: {result=}')
    
        again = get_yes_no('Another game? ')
        if again != 'y':
            break

try:
    main()
except KeyboardInterrupt:
    print()


