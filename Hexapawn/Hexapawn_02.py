"""
An implementation of Hexapawn.  Version 02.

Extend the game from version 01.
Add functions and code to handle mirror positions that aren't in the computer
move tree.
"""

import random
from copy import deepcopy
from Hexapawn_00 import MoveTree


# the initial board
InitialBoard = [['B', 'B', 'B'],
                [' ', ' ', ' '],
                ['W', 'W', 'W']
               ]

# This is the "working" copy of MoveTree that is modified during learning
# Must be copied from MoveTree using a deep copy
GameState = None

# the game board, modified from InitialBoard
GameBoard = None


def get_yes_no(prompt):
    """Get a yes/no response from the player.

    Returns 'y' or 'n'.
    """

    print()
    while True:
        response = input(prompt).lower()[:1]
        if response in 'yn':
            return response
        print('Sorry, only YES or NO.')

def print_board(board):
    """Print the board."""

    print()
    print('    0   1   2')
    print('  +---+---+---+')
    print(f'0 | {board[0][0]} | {board[0][1]} | {board[0][2]} |')
    print('  +---+---+---+')
    print(f'1 | {board[1][0]} | {board[1][1]} | {board[1][2]} |')
    print('  +---+---+---+')
    print(f'2 | {board[2][0]} | {board[2][1]} | {board[2][2]} |')
    print('  +---+---+---+')
    print()

def get_board_position(board):
    """Return the "position string" from the board."""

    position = []
    for y in range(3):
        for x in range(3):
            position.append(board[y][x])
    return ''.join(position)

def get_player_move():
    """Get a player move.

    Returns (x1, y1, x2, y2) meaninf move from position (x1,y1)
    to position (x2,y2).

    Doesn't check if move is legal for the position.
    """

    while True:
        move_str = input('Player move: ')
        if not move_str:
            continue
        move = move_str.split()
        if len(move) != 2:
            print("Sorry, a move is something like '0,1 1,1'.")
            continue
        (start, end) = move
        start = start.split(',')
        end = end.split(',')

        if start[0] not in '012' or start[1] not in '012':
            print("Sorry, a move is something like '0,1 1,1'.")
            continue
        if end[0] not in '012' or end[1] not in '012':
            print("Sorry, a move is something like '0,1 1,1'.")
            continue

        return (int(start[0]), int(start[1]), int(end[0]), int(end[1]))

def legal_move(board, move):
    """Check if a player move is legal for the board state."""

    (startx, starty, endx, endy) = move

    # is the start position 'W'?
    if board[starty][startx] != 'W':
        return False

    # is the end position blank or 'B'?
    if board[endy][endx] not in ' B':
        return False

    # is the move one square up or diagonal
    if startx == endx:
        # UP move, only one square?
        if endy != starty - 1:
            return False
    elif (endx == startx + 1) or (endx == startx - 1):
        # DIAGONAL, only one square up?
        if endy != starty - 1:
            return False
    else:
        return False

    return True

def update_board(board, move):
    """Update board with a move."""

    (startx, starty, endx, endy) = move

    colour = board[starty][startx]
    board[starty][startx] = ' '
    board[endy][endx] = colour

def legal_player_moves(board):
    """Return 'True' if there are any legal player moves."""

    for x in range(3):
        for y in range(3):
            if board[y][x] == 'W':
                if y != 0:
                    if board[y-1][x] == ' ':
                        return True
                    if x > 0:
                        if board[y-1][x-1] == 'B':
                            return True
                    if x < 2:
                        if board[y-1][x+1] == 'B':
                            return True

    return False

def position_reverse(position):
    """Reverse the given position string."""

    return position[:3][::-1] + position[3:6][::-1] + position[6:][::-1]

xmirror = {0: 2, 1: 1, 2: 0}

def move_reverse(move):
    """Reverse the given move."""

    (fromx, fromy, tox, toy) = move
    return (xmirror[fromx], fromy, xmirror[tox], toy)

def moves_reversed(moves):
    """Reverse a *list* of moves."""

    return [move_reverse(m) for m in moves]

def equal_moves(a, b):
    """Determine if two lists of moves are identical."""

    return sorted(a) == sorted(b)

def winner(board):
    """Determine if there is a winner."""

    if 'W' in (board[0][0], board[0][1], board[0][2]):
        return 'W'
    if 'B' in (board[2][0], board[2][1], board[2][2]):
        return 'B'

    return None

def get_computer_move(position, state):
    """Get a random computer move."""

    moves = state.get(position, None)
    if not moves:
        # no immediate move, check mirror position
        position = position_reverse(position)
        moves = state.get(position, None)
        if not moves:
            # no reversed move either, so no possible moves
            return None
        move = random.choice(moves)
        # save last move
        return move_reverse(move)

    return random.choice(moves)

def play_one_game(board, state):
    print()
    print_board(board)

    while True:
        # see if player has ANY legal moves
        if not legal_player_moves(board):
            print('White cannot move!')
            return 'B'

        # get legal player move
        while True:
            pmove = get_player_move()
            if legal_move(board, pmove):
                break
            print("Sorry, that isn't a legal W move.")

        # update board with player move
        update_board(board, pmove)
        print_board(board)

        # a win?
        if colour := winner(board):
            return colour

        # get computer move - must handle mirrored positions
        position = get_board_position(board)
        cmove = get_computer_move(position, state)
        print('Black moves')
        if cmove is None:
            print('Black cannot move!')
            return 'W'

        # update board with computer move
        update_board(board, cmove)
        print_board(board)

        # a win?
        if colour := winner(board):
            return colour

def main(white_wins, black_wins):
    """the game loop."""

    while True:
        GameState = deepcopy(MoveTree)
        GameBoard = deepcopy(InitialBoard)
    
        result = play_one_game(GameBoard, GameState)
        if result is not None:
            if result == 'W':
                print('White wins!')
                white_wins += 1
            else:
                print('Black wins!')
                black_wins += 1
        else:
            print('A draw!')
    
        again = get_yes_no('Another game? ')
        if again != 'y':
            break

    # return game state
    return (white_wins, black_wins)

###########################################

# get saved game state
# MoveTree = ??
white_wins = 0
black_wins = 0

try:
    (white_wins, black_wins) = main(white_wins, black_wins)
    print(f'White wins: {white_wins}')
    print(f'Black wins: {black_wins}')
except KeyboardInterrupt:
    print()

