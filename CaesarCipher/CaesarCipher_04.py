shift = 3                                   # the Caesar shift number
characters = "abcdefghijklmnopqrstuvwxyz"   # the characters to encode
ch_len = len(characters)

# create the encode dictionary
encode_dict = {}                            # initially empty decode dictionary
for (ch_index, ch) in enumerate(characters):# cycle through all characters
    new_index = ch_index + shift            #     add the shift
    if new_index >= ch_len:                 #     handle "wrap around"
        new_index = new_index - ch_len
    encode_dict[ch] = characters[new_index] #     add key:value pair to the dictionary

# now invert the encode dictionay to get the decode dictionary
decode_dict = {v: k for (k, v) in encode_dict.items()}

# encode a user message
msg = input('Message to encode: ')          # message to encode
encoded_msg = ""                            # the decoded result
for ch in msg:                              # for each character in the message text
    new_char = encode_dict.get(ch, ch)      #     get decoded character, not found just pass through
    encoded_msg = encoded_msg + new_char    # save encoded character
print(f'Encoded message={encoded_msg}')     # print final encoded message

# decode the encoded message from above and test same as original
decoded_msg = ""                            # the decoding of the encoded message
for ch in encoded_msg:
    new_char = decode_dict.get(ch, ch)
    decoded_msg = decoded_msg + new_char
print(f'Decoded message={decoded_msg}')
if decoded_msg != msg:
    print('ERROR: decode not the same as original!')
else:
    print('Encode/decode resulted in the same message.')
