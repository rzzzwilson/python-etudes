# Using string positions

The ord/chr method is fine but we are limited to character sequences that
are contiguous in the ASCII table.  We could handle lowercase and uppercase
characters as two separate sequences in ASCII but trying to handle spaces
and punctuation gets very messy.

Another approach is to take a string of characters to be encoded from the 
user and use the position of each character to encode in that string and
do the addition+wrap to get the encoded character position in the string.

## Code

That's pretty easy to do and the code is roughly the same as in the
ord/chr code:

    shift = 3                                   # the Caesar shift number
    encoded_msg = ""                            # the encoded result
    characters = "abcdefghijklmnopqrstuvwxyz0123456789"
    ch_len = len(characters)                    # length of the character string
    
    msg = input('Message to encode: ')          # message to encode
    
    for ch in msg:                              # for each character in the message text
        position = characters.find(ch)          #     look for its position in "characters"
        if position == -1:                      #         if position is -1, not found
            new_char = ch                       #             pass through unchanged
        else:
            new_value = position + shift        #     else add the shift
            if new_value >= ch_len:             #     handle "wrap around"
                new_value = new_value - ch_len  #
            new_char = characters[new_value]    #     convert back to enciphered character
        encoded_msg = encoded_msg + new_char    # save encoded character
    
    print(f'Encoded message={encoded_msg}')     # print final encoded message

Note that for the first time we pass through unchanged any character that is not
in the "encodable" characters string.  It's better to do this than just assume
the string to encode contains only characters we can encode.

The code to do this is in [this file](CaesarCipher_02.py).

[On the next page](CaesarCipher_03.md) we decode strings produced by the character
position method.
