shift = 3                               # the Caesar shift number
decoded_msg = ""                        # the decoded result

msg = input('Message to decode: ')      # message to decode

for ch in msg:                          # for each character in the message text
    value = ord(ch)                     #     convert to number
    new_value = value - shift           #     SUBTRACT the shift
    if new_value < ord('a'):            #     handle "wrap around"
        new_value = new_value + 26
    new_char = chr(new_value)           #     convert back to enciphered character
    decoded_msg = decoded_msg + new_char# save encoded character

print(f'Encoded message={decoded_msg}') # print final encoded message
