# Caesar Cypher

This etude was prompted by a reddit/com/r/learnpython question:

    https://old.reddit.com/r/learnpython/comments/ki10ex/i_am_creating_a_caesar_cipher_for_a_project_and_i/

To which my answer was:

One way to do this is to create a string containing all the letters you want to
encode. Suppose you want to encode lowercase letters:

    alphabet = 'abcdefghijklmnopqrstuvwxyz'

Now, a Caesar cypher encodes a letter by replacing it with the letter N places
to the right. So if N is 3 and we want to encode "d" we look three places to the
right of "d" in the string and find "g".

The way to do this in python is to take the letter to encode ("d") and find its
position in the alphabet string. We use the string method find() to do this:

    index = alphabet.find('d')

This returns the "index" into the string of 3 for the letter "d". Now we add the
N value to index, getting 6. The sixth character (starting at 0) in the string
is alphabet[6] which is the letter "g".

That's the basic idea. You will also have to think about how to handle cases
such as where N is 3 and you want to encode the letter "y". Hint: modulo.

## This etude

This etude will expand on my answer in /r/learnpython and consider other
approaches beside that mentioned above.  [It start here](CaesarCipher_00.md).
