# Introduction

Here we discuss one of the common, simple, problems that is often posed to 
those learning python:

    Construct a program that will encode and decode text with a Caesar cipher
    given the text of the message and the offset *N* used to encode and decode
    the message.  

A [Caesar cipher is described here](https://en.wikipedia.org/wiki/Caesar_cipher).

# What we will cover

A Ceasar cipher can be implemented in some basic ways:

* Using the ord() and chr() python builtin functions,
* Use a string and position in string rather than ord() or chr(),
* Use a dictionary to map plain to encoded characters

## ord() and chr()

Python has two [builtin functions, *ord()* and *chr()*](https://docs.python.org/3/library/functions.html).  

The *ord()* function takes a one character string and returns the underlying
numeric value for the character.  The *chr()* function performs the opposite
operation, converting an integer value to the character represented by the
value.

Using these two functions we can perform arithmetic on the underlying numeric
values to perform the cipher "shift".

## String Position

An approach related to the ord()/chr() approach above is to take a string of
characters to encrypt for the user.  We use the character positions
in the string rather than the ord() values to do our encryption.  This is a
little more general then using ord() and chr().

## Dictionaries

It is possible to use a dictionary to perform Caesar encoding.  There is some
initial setup that you have to do (creating the dictionary) but after that
encoding and decoding is fast.  We will discuss this after using the two
approaches above.

# Caesar cipher using ord() and chr()

As mentioned, a character in a string is really just a numeric value that is
displayed as a character.  We can see what the numeric value of a character is
by using the *ord()* builtin function.  The following code:

    msg = 'abcdxyz'
    for ch in msg:
        print(f'ch={ch}, {ord(ch)=}')

prints this when run:

    ch=a, ord(ch)=97
    ch=b, ord(ch)=98
    ch=c, ord(ch)=99
    ch=d, ord(ch)=100
    ch=x, ord(ch)=120
    ch=y, ord(ch)=121
    ch=z, ord(ch)=122

We see that the letter "a" has a value of 97 (decimal).  It's easy to get the
integer value of a character.  The other important point is that it looks like
the value of "b" is one more than "a", the value of "c" is one more than "b",
and so on.  If you look at an [ASCII table](https://en.wikipedia.org/wiki/ASCII)
you will see that the lowercase letters are arranged in alphabetical order in
the table and each succeeding character has a value one more than the preceding
character.

And that's important for implementing a Caesar cipher.  We can take the value
for "a", for example, and if the cipher shifts encoded characters three places
to the right all we have to do is add three to the value of "a" (97 decimal)
to get 100 and use the chr() function to convert 100 back to the appropriate
character, "d" in this case.

All we have to worry about is the case where adding the Caesar shift to a
character takes it past the last lowercase character "z".  In that case we just
subtract the length of the run of lowercase characters (26) from the number
before using chr() to get the encoded character.  For example, if we are
encoding the character "y" and the shift is three the ord() function gives 
us 121 decimal and adding 3 gets 124.  That's past the 
number for "z" so we have to subtract the number of lowercase characters, which
is 26.  That means the encoded number is *124 - 26* which is 98.  Using chr(98)
we get "b" which is correct.  Now let's write this code:

    msg = 'abcxyz'                      # message to encipher
    shift = 3                           # the Caesar shift
    for ch in msg:                      # for each character in the message text
        value = ord(ch)                 # convert to number
        new_value = value + shift       # add the shift
        if new_value > ord('z'):        # handle "wrap around"
            new_value = new_value - 26
        new_char = chr(new_value)       # convert back to enciphered character
        print(f'ch={ch}, encoded={new_char}')

The code will encipher each character of the message string and print each
enciphered character:

    $ python3 CaesarCipher.00.py
    ch=a, encoded=d
    ch=b, encoded=e
    ch=c, encoded=f
    ch=x, encoded=a
    ch=y, encoded=b
    ch=z, encoded=c

## Encode a string

We would like to be able to encode any single string from the user, so we change
the code to ask for input from the user and display the encoded text in one
string:

    shift = 3                               # the Caesar shift number
    encoded_msg = ""                        # the encoded result
    
    msg = input('Message to encode: ')      # message to encode
    
    for ch in msg:                          # for each character in the message text
        value = ord(ch)                     #     convert to number
        new_value = value + shift           #     add the shift
        if new_value > ord('z'):            #     handle "wrap around"
            new_value = new_value - 26
        new_char = chr(new_value)           #     convert back to enciphered character
        encoded_msg = encoded_msg + new_char# save encoded character
    
    print(f'Encoded message={encoded_msg}') # print final encoded message

Running this code and encoding a message looks like this:

    $ python3 CaesarCipher.00.py
    Message to encode: thequickbrownfoxjumpsoverthelazydog                     
    Encoded message=wkhtxlfneurzqiramxpsvryhuwkhodcbgrj

We must be careful to only have lowercase alphabetic characters in our message
to encode because the code we have written only handles lowercase characters.
We will remove this limitation later.

The code to do this is in [this file](CaesarCipher_00.py).

[On the next page](CaesarCipher_01.md) we will use *ord()* and *chr()* to
decode an enciphered message.
