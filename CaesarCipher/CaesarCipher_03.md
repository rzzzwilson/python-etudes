# Decoding with string positions

Decoding a string encoded using string positions is very simple.
We use the same "characters" string and almost the same code:

    shift = 3                                   # the Caesar shift number
    encoded_msg = ""                            # the encoded result
    characters = "abcdefghijklmnopqrstuvwxyz0123456789"
    ch_len = len(characters)                    # length of the character string
    
    msg = input('Message to decode: ')          # message to decode
    
    for ch in msg:                              # for each character in the message text
        position = characters.find(ch)          #     look for its position in "characters"
        if position == -1:                      #         if position is -1, not found
            new_char = ch                       #             pass through unchanged
        else:
            new_value = position - shift        #     else add the shift
            if new_value < 0:                   #     handle "wrap around"
                new_value = new_value + ch_len  #
            new_char = characters[new_value]    #     convert back to enciphered character
        encoded_msg = encoded_msg + new_char    # save encoded character
    
    print(f'Encoded message={encoded_msg}')     # print final encoded message

## Testing

Using the two "string position" programs we can test our code by encoding a string
and seeing if we get the same string when decoding:

    $ python CaesarCipher_02.py
    Message to encode: The quick brown fox jumps over the lazy dog. 
    Encoded message=Tkh txlfn eurzq ir0 mxpsv ryhu wkh od21 grj.
    $ python CaesarCipher_03.py
    Message to decode: Tkh txlfn eurzq ir0 mxpsv ryhu wkh od21 grj.
    Encoded message=The quick brown fox jumps over the lazy dog.

Everything is working!

The code to do this is in [this file](CaesarCipher_03.py).

[On the next page](CaesarCipher_04.md) we use dictionaries to encode/decode.
