shift = 3                               # the Caesar shift number
encoded_msg = ""                        # the encoded result

msg = input('Message to encode: ')      # message to encode

for ch in msg:                          # for each character in the message text
    value = ord(ch)                     #     convert to number
    new_value = value + shift           #     add the shift
    if new_value > ord('z'):            #     handle "wrap around"
        new_value = new_value - 26
    new_char = chr(new_value)           #     convert back to enciphered character
    encoded_msg = encoded_msg + new_char# save encoded character

print(f'Encoded message={encoded_msg}') # print final encoded message
