# Using dictionaries

The previous methods are fine, especially the "string position" approach.
Now we show how to use dictionaries to encode and decode strings.  This uses
a "string position" approach to assign numbers to the characters.

A dictionary to encode lowercase characters with a shift of 3 will look like
this:

    encode_dict = {'a': 'd', 'b': 'e', ..., 'z': c}

It's clear that the dictionary key is the character to encode and the value
matching the key is the encoded character.  How do we create the dictionary,
given a string of characters and the Caesar shift of 3?

The code to do this is similar to the initial encode method using the
character position in the "characters" string:

    shift = 3                                   # the Caesar shift number
    characters = "abcdefghijklmnopqrstuvwxyz"   # the characters to encode
    ch_len = len(characters)
    
    # create the encode dictionary
    encode_dict = {}                            # initially empty decode dictionary
    for (ch_index, ch) in enumerate(characters):# cycle through all characters
        new_index = ch_index + shift            #     add the shift
        if new_index >= ch_len:                 #     handle "wrap around"
            new_index = new_index - ch_len
        encode_dict[ch] = characters[new_index] #     add key:value pair to the dictionary

Note that there is quite a bit of code to set up the encoding dictionary, but
once that is done the code to encode the message is shorter that it was before.
Encoding a string is easy, we just take care that if a characteer to decode is
not in the dictionary we just pass it through:

    msg = input('Message to encode: ')          # message to encode
    encoded_msg = ""                            # the decoded result
    for ch in msg:                              # for each character in the message text
        new_char = encode_dict.get(ch, ch)      #     get decoded character, not found just pass through
        encoded_msg = encoded_msg + new_char    # save encoded character
    print(f'Encoded message={encoded_msg}')     # print final encoded message

# Decoding using a dictionary

To decode we just need to create a decode dictionary that does the opposite.
We *could* use similar code to that creating the encode dictionary, but there
is a better way.

We can **invert** the encode dictionary.  Inverting a dictionary means taking
an existing dictionary and creating a new dictionary which has keys that were
values in the original dictionary, and has values that were originally keys.
You need to be careful when inverting dictionaries.  If there are values in the
original dictionary that are the same as other values in the original then you 
will lose data, because a repeated key will delete the first occurrence of that
key.  Happily, in a Caesar cipher we know that repeated values can't occur.

To invert the encoding dictionary we do this, a dictionary comprehension:

    decode_dict = {v: k for (k, v) in encode_dict.items()}
    #              ^^^^

Note that the key/value definition in the comprehension is "inverted", that is,
the new key is the old value and the new value is the old key.

# Final code and testing

Now we can modify the code we already have to include the "inverting" code
and to automatically decode the message we encode.  This way it's easy to
check that the encode/decode is working correctly:

When we run that code and enter our message, we see:

    $ python3 CaesarCipher_04.py
    Message to encode: The quick brown fox jumps over the lazy dog.
    Encoded message=Tkh txlfn eurzq ira mxpsv ryhu wkh odcb grj.
    Decoded message=The quick brown fox jumps over the lazy dog.
    Encode/decode resulted in the same message.

The decoded message is exactly the same as the original message.  Note that
characters that aren't in the "characters" string, like the "T", fullstop and spaces
are passed through unchanged.  So everything looks like it's working!

The code to do this is in [this file](CaesarCipher_04.py).

# Finally

What we have seen is just the basic code running.  A nice usable solution would
be a library where you initialize with any "character" string and Caesar shift
number to produce something that can be used to encode and decode many user-supplied
strings.
