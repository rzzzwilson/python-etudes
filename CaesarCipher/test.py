import string

class Caesar:
    """Create a Ceasar cypher instance that can encode/decode strings
    using a defined "N" offset.
    """

    def __init__(self, offset, alphabet=string.ascii_lowercase):
        # make sure "offset" is within the alphabet size
        if not 0 <= offset < len(alphabet):
            raise ValueError(f'Bad value for offset: {offset}')

        # construct encode dictionary
        self.encode_dict= {x:y for (x,y) in zip(alphabet, alphabet[offset:]+alphabet)}

        # now invert the "encode" dictionary to get "decode" dictionary
        self.decode_dict = {v:k for (k,v) in self.encode_dict.items()}

    def encode(self, msg):
        new_msg = []
        for ch in msg:
            new_msg.append(self.encode_dict.get(ch, ch))
        return ''.join(new_msg)

    def decode(self, msg):
        new_msg = []
        for ch in msg:
            new_msg.append(self.decode_dict.get(ch, ch))
        return ''.join(new_msg)

n = 3
alphabet = string.ascii_letters + ' ' + string.digits + string.punctuation + '\n'
c = Caesar(n, alphabet=alphabet)

#msg = 'The quick brown fox jumps over the lazy dog'
#msg = 'The quick brown fox jumped over the lazy dog 42 times'
#msg = 'The quick brown fox jumped over the lazy dog 42 times!'
msg = '''
The time has come, the Walrus said,
      To talk of many things:
Of shoes - and ships - and sealing-wax -
      Of cabbages - and Kings -
And why the sea is boiling hot -
      And whether pigs have wings.

Excerpt from "The Walrus and the Carpenter", by Lewis Carrol, 1871.'''

encoded_msg = c.encode(msg)
print(f'encoded msg={encoded_msg}')
decoded_msg = c.decode(encoded_msg)
print(f'decoded msg={decoded_msg}')

