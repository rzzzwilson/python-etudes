# Decoding messages

Now that we can encode messages, it is probably time we work out how to decode
an already encoded message.  We need an encoded message to work with, so we
use the program we wrote on the previous page:

    $ python3 CaesarCipher.00.py
    Message to encode: thequickbrownfoxjumpsoverthelazydog                     
    Encoded message=wkhtxlfneurzqiramxpsvryhuwkhodcbgrj

Decoding a message is pretty much as before, except this time we have to undo
the shift of 3 characters.  The "overflow" test is done to see if the decoded
character is less than "a".  We add 26 in that case:

    shift = 3                               # the Caesar shift number
    decoded_msg = ""                        # the decoded result
    
    msg = input('Message to decode: ')      # message to decode
    
    for ch in msg:                          # for each character in the message text
        value = ord(ch)                     #     convert to number
        new_value = value - shift           #     SUBTRACT the shift
        if new_value < ord('a'):            #     handle "wrap around"
            new_value = new_value + 26
        new_char = chr(new_value)           #     convert back to enciphered character
        decoded_msg = decoded_msg + new_char# save encoded character
    
    print(f'Encoded message={decoded_msg}') # print final encoded message

Now if we decode the encoded message, we get:

    $ python3 CaesarCipher.01.py
    Message to decode: wkhtxlfneurzqiramxpsvryhuwkhodcbgrj
    Encoded message=thequickbrownfoxjumpsoverthelazydog

Which is what we expected.

The code to do this is in [this file](CaesarCipher_01.py).

[On the next page](CaesarCipher_02.md) we will use string positions to encode
and decode characters.
