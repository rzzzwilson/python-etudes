shift = 3                                   # the Caesar shift number
encoded_msg = ""                            # the encoded result
characters = "abcdefghijklmnopqrstuvwxyz0123456789"
ch_len = len(characters)                    # length of the character string

msg = input('Message to decode: ')          # message to decode

for ch in msg:                              # for each character in the message text
    position = characters.find(ch)          #     look for its position in "characters"
    if position == -1:                      #         if position is -1, not found
        new_char = ch                       #             pass through unchanged
    else:
        new_value = position - shift        #     else add the shift
        if new_value < 0:                   #     handle "wrap around"
            new_value = new_value + ch_len  #
        new_char = characters[new_value]    #     convert back to enciphered character
    encoded_msg = encoded_msg + new_char    # save encoded character

print(f'Encoded message={encoded_msg}')     # print final encoded message
