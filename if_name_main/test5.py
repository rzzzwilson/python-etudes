def greet(name):
    """Function to greet person with name "name"."""

    print('Hello,', name)

if __name__ == '__main__':
    import sys              # only needed when execute directly
    greet(sys.argv[1])

