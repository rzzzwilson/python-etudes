import sys

def greet(name):
    """Function to greet person with name "name"."""

    print('Hello,', name)

greet(sys.argv[1])
