What is "if __name__ == '__main__': for?"
=========================================

Following a question in the **/r/learnpython** subreddit, here is a short
spiel on the common idiom::

    if __name__ == '__main__':

__name__
--------

Suppose we have a small file *test.py*::

    print('In test.py: __name__ =', __name__)

When we execute this file from the commandline we see::

    $ python test.py
    In test.py: __name__ = __main__

This shows that when executed directly (not imported) the name `__name__`
in the file being executed is `__main__`.

But if we create another file *test2.py* which imports *test.py*::

    import test
    print('In test2.py: __name__ =', __name__)

we see::

    $ python test2.py
    In test.py: __name__ = test
    In test2.py: __name__ = __main__

The file we are directly executing (*test2.py*) still has the `__name__`
variable containing `__main__` and the imported file *test.py* has `__name__`
set to `test`.

The general rule is:

* a directly executed file has `__name__` set to `__main__`
* an imported file has `__name__` set to the name of the imported file

Why use "if __name__ == '__main__':"?
-------------------------------------

Suppose we have a small program that is designed to greet a person::

    import sys
    
    def greet(name):
        """Function to greet person with name "name"."""
    
        print('Hello,', name)
    
    greet(sys.argv[1])  # use first user param from the commandline

The code is designed to be run from the commandline like this::

    $ python test3.py Julia
    Hello, Julia

The code isn't very good.  If we don't supply a name it crashes, but it's
just an example.

This is really useful, you decide, so you want other people to use it.
But when we try importing the file like this::

    import sys
    import test3
    
    # use the greet() function from test3.py
    test3.greet(sys.argv[1])

we see this when executing the code from the command line::

    $ python test4.py Tom
    Hello, Tom
    Hello, Tom

Why do we get **two** prints!?

You need to remember that when you import a file the code in that file is
executed from top to bottom.  So when importing *test3.py*::

    import sys
    
    def greet(name):
        """Function to greet person with name "name"."""
    
        print('Hello,', name)
    
    greet(sys.argv[1])

the function `greet()` is defined and then we call `greet()` passing the name
from the command line.  After that we continue with the lines after the 
import in *test4.py*::

    import sys
    import test3        # greet() called in test3.py
    
    test3.greet(sys.argv[1])

which again executes a call to *greet()*.

To get around this we use the `if __name__ == '__main__':` idiom.
The code in *test3.py* contains the function we want people to import
as well as a call to the function.  We want the function definition
to occur when the file is imported but **not** the `greet()` call.
So we write a new file *test5.py* like this::

    def greet(name):
        """Function to greet person with name "name"."""
    
        print('Hello,', name)
    
    if __name__ == '__main__':
        import sys              # only needed when executed directly
        greet(sys.argv[1])

Now when we directly execute *test5.py* we see::

    $ python test5.py Julia
    Hello, Julia

If we write *test6.py* that imports *test5.py*::

    import sys
    import test5
    
    test5.greet(sys.argv[1])

we see this when executing *test6.py*::

    $ python test6.py Tom
    Hello, Tom

Wrapup
======

The takeaway from this is that you need `if __name__ == '__main__':` in a file
only when you *import* the file and you don't want some code in that file to be
executed.  The `if` statement is a standard python `if` statement.  The only
*magic* is in python's handling of the `__name__` global.

