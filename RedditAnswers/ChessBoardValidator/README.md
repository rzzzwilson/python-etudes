Chess Board Validator
=====================

This problem was mentioned in the
[/r/learnpython subreddit](https://www.reddit.com/r/learnpython/comments/feny7x/how_difficult_are_the_exercises_in_automate_the/).

It's from [Automate The Boring Stuff](https://automatetheboringstuff.com/),
chapter 5:

> In this chapter, we used the dictionary value
> {'1h': 'bking', '6c': 'wqueen', '2g': 'bbishop', '5h': 'bqueen', '3e': 'wking'}
> to represent a chess board. Write a function named isValidChessBoard() that takes
> a dictionary argument and returns True or False depending on if the board is valid.
> 
> A valid board will have exactly one black king and exactly one white king. Each
> player can only have at most 16 pieces, at most 8 pawns, and all pieces must be
> on a valid space from '1a' to '8h'; that is, a piece can’t be on space '9z'. The
> piece names begin with either a 'w' or 'b' to represent white or black, followed
> by 'pawn', 'knight', 'bishop', 'rook', 'queen', or 'king'. This function should
> detect when a bug has resulted in an improper chess board.

How to do this in python?

Approach
--------

The basic idea is to just write an empty *isValidChessBoard()* function that
always returns *False* and then write some test code.  We put the code into
*chess_board_validator.py*:

    def isValidChessBoard(board):
        return False

The test code, using *unittest*, looks like this:

    import unittest
    from chess_board_validator import isValidChessBoard
    
    class TestisValidChessBoard(unittest.TestCase):
    
        def test_empty(self):
            """Check that an empty board passes."""
    
            board = {}
            result = isValidChessBoard(board)
            msg = "Empty board validated False?"
            self.assertTrue(result, msg)

    unittest.main()

When we run the unit tests we see:

    $ python3 test_chess_board_validator.py
    F
    ======================================================================
    FAIL: test_empty (__main__.TestisValidChessBoard)
    Check that an empty board passes.
    ----------------------------------------------------------------------
    Traceback (most recent call last):
      File "test_chess_board_validator.py", line 12, in test_empty
        self.assertTrue(result, msg)
    AssertionError: False is not true : Empty board validated False?
    
    ----------------------------------------------------------------------
    Ran 1 test in 0.000s
    
    FAILED (failures=1)

That is as expected, since we haven't implemented the *isValidChessBoard()*
function yet.  We have, however, tested that the unittest system is working!

First Test
----------

The basic idea is that we step through all positions in the given board
dictionary and check the data we find.  The first thing we do is check that the 
position square (the dict key) is valid.  This check is easy, we just split the
two character string that is the square coordinates and check if each character
is legal:

    def isValidChessBoard(board):
        for (square, name) in board.items():
            # check that the square is valid
            try:
                (row, col) = square
            except ValueError:
                return False        # wasn't two characters!
            if row not in '12345678' or col not in 'abcdefgh':
                return False
    
        return True                 # if we get here the board is legal

We create an outer loop stepping through each *(square, piece)* item
in the dictionary.  We will run all our main tests in this loop.

We add another test case to check square positions:

    def test_bad_square(self):
        """Check that an non-empty invalid board doesn't pass (bad square)."""

        board = {'9a':'wpawn'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with bad square validated True?"
        self.assertFalse(result, msg)

Now when we run the test code we don't get an error:

    $ python3 test_chess_board_validator.py
    ..
    ----------------------------------------------------------------------
    Ran 2 tests in 0.000s
    
    OK

Check Piece Name Is Valid
-------------------------

Now we test that the *name* of the piece is valid.  This is most easily done
by checking that the name from the dictionary is in a list of valid names.  We
call this valid names list *valid_names* (yes, really original!) and define it
outside the function:

    valid_names = ['wpawn', 'wrook', 'wknight', 'wbishop', 'wqueen', 'wking',
                   'bpawn', 'brook', 'bknight', 'bbishop', 'bqueen', 'bking']
    
    def isValidChessBoard(board):
        for (square, name) in board.items():
            # check that the square is valid
            try:
                (row, col) = square
            except ValueError:
                return False        # wasn't two characters!
            if row not in '12345678' or col not in 'abcdefgh':
                return False
    
            # check the name is valid
            if name not in valid_names:
                return False
    
        return True                 # if we get here the board is legal

Actually, *valid_names* doesn't have to be a list, any simple sequence will do,
even a tuple.  We just want something that a simple `in` test will check.

We also add a few more test cases to the unittest code.  We check a non-empty
valid board and also a board with a bad piece name:

    import unittest
    from chess_board_validator import isValidChessBoard

    class TestisValidChessBoard(unittest.TestCase):
    
        def test_empty(self):
            """Check that an empty board passes."""
    
            board = {}
            result = isValidChessBoard(board)
            msg = "Empty board validated False?"
            self.assertTrue(result, msg)

        def test_valid(self):
            """Check that an non-empty valid board passes."""

            board = {'2a':'wpawn'}
            result = isValidChessBoard(board)
            msg = "Non-empty valid board validated False?"
            self.assertTrue(result, msg)

        def test_bad_name(self):
            """Check that an non-empty invalid board doesn't pass (bad name)."""
    
            board = {'2a':'qpawn'}
            result = isValidChessBoard(board)
            msg = "Non-empty valid board with bad name validated True?"
            self.assertFalse(result, msg)

        def test_bad_square(self):
            """Check that an non-empty invalid board doesn't pass (bad square)."""
    
            board = {'9a':'wpawn'}
            result = isValidChessBoard(board)
            msg = "Non-empty valid board with bad square validated True?"
            self.assertFalse(result, msg)

    unittest.main()

The tests all look good:

    $ python test_chess_board_validator.py
    ...
    ----------------------------------------------------------------------
    Ran 4 tests in 0.000s
    
    OK

From now on we won't be showing all the code in either file.

Next Test
---------

Next we check that we haven't seen too many of each piece.  If we see three white
bishops then the board is invalid!  We initialize a dictionary holding the count
of each piece we have scanned.  We also add a dictionary outside the function
that maps the piece name to the maximum number of pieces allowed for each type:

    # dict holding maximum numbers of each piece
    max_names = {'wpawn':8, 'wrook':2, 'wknight':2, 'wbishop':2, 'wqueen':1, 'wking':1,
                 'bpawn':8, 'brook':2, 'bknight':2, 'bbishop':2, 'bqueen':1, 'bking':1}
    
    def isValidChessBoard(board):
        # initialize dict holding piece counters
        piece_count = {'wpawn':0, 'wrook':0, 'wknight':0, 'wbishop':0, 'wqueen':0, 'wking':0,
                       'bpawn':0, 'brook':0, 'bknight':0, 'bbishop':0, 'bqueen':0, 'bking':0}
    
    
        for (square, name) in board.items():
            # all previous tests
    
            # update count of times we've seen this piece name
            piece_count[name] += 1
            if piece_count[name] > max_names[name]:
                return False
    
        return True

We also add a new test case:

    def test_too_many_knights(self):
        """Check that an non-empty invalid board doesn't pass (too many knights)."""

        board = {'2a':'qpawn', '1a':'wknight', '2c':'wknight', '3c':'wknight'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with too many knights validated True?"
        self.assertFalse(result, msg)

No error from the tests.

Final Test?
-----------

We appear to be almost finished.  Now we just need to check the the bishops
don't "clash" over colour.  Each white bishop sticks to one colour of square.
There will be a white bishop on white squares and the other white bishop will
be on black squares.

We check this by having a sort of special case for bishops.  A special dictionary
*bishop_colours* has a key of a tuple of the bishop colour and the square colour
that particular bishop is on.  So when we see a white bishop on a black square
we check the dictionary for the value associated with the tuple
*('white', 'black')*.  If that value is already 1 then the board is invalid
because we have two bishops on the same colour.

Here is the bishop counter dictionary.  We define it inside the function because
we must start with the count values set to zero:

    # initialize counters of bishop square colours
    # key tuple is (bishop colour, square colour)
    bishop_colours = {('white', 'white'): 0,
                      ('white', 'black'): 0,
                      ('black', 'white'): 0,
                      ('black', 'black'): 0}

Now, we have a minor problem.  We can find the piece colour from its name,
but we can't easily find the colour of the square at position "1a", say.
We write a function that will map the two character position string "1a"
to the square colour ("black" or "white").  That function is:

    # function mapping square string ('1a') to a string ('black' or 'white')
    def square_colour(square):
        # convert square string to numeric coordinates
        (row, col) = square
        row = int(row) - 1      # both coords zero based
        col = 'abcdefgh'.index(col)
        if row % 2:
            return 'black' if col % 2 else 'white'
        return 'white' if col % 2 else 'black'

The new code in the main function is:

        # check bishop square colour is OK
        if name in ['wbishop', 'bbishop']:
            sq_colour = square_colour(square)   # returns 'white' or 'black'
            p_colour = name[0]                  # get piece colour
            p_colour = 'white' if p_colour == 'w' else 'black'
            if bishop_colours[(p_colour, sq_colour)] > 0:
                return False
            bishop_colours[(p_colour, sq_colour)] = 1

As before, we also write another test to check the new code:

    def test_same_bishop_square_colour(self):
        """Check that an non-empty invalid board doesn't pass (two bishps, same square colour)."""

        board = {'1a':'bbishop', '8h':'bbishop'}    # both on same diagonal
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with two bishops same square colour validated True?"
        self.assertFalse(result, msg)

All tests pass!

Cleanup
-------

We can do a little cleanup.

When we started we defined a list *valid_names* which we used to check if a
piece name is valid.  Since then we have defined a couple of dictionaries
that we could use instead.  The *in* operator tests if a value is a key in
athe dictionary.  So we can delete the *valid_names* list and used the 
*piece_count* dictionary instead since it has all the valid piece names
as the key.  We do so, and the tests still pass, which shows how useful
tests can be.  We just made a change to the code but we quickly showed
that the changed code is still behaving correctly!

Things That Need To Be Considered
---------------------------------

The code looks good, but after more thought on what makes a "legal" chess board
it is obvious that we need to add more tests.

There *must* be one king of each colour.  That's another test.

A pawn reaching the far row can be promoted to another piece.  The rules say:

    Promotion in chess is a rule that requires a pawn that reaches its eighth
    rank to be replaced by the player's choice of a queen, knight, rook, or
    bishop of the same color.

This means that there can be more than the current maximum number of queen,
knight, rook, or bishop pieces on the board.  Obviously, there can be more than
one queen after a promotion to queen.  So we need to add code that will allow
for that eventuality.  Since each side starts with eight pawns there is an upper
limit to the number of possible queen, knight, rook, or bishop pieces.

In addition, promotion to bishop may result in two bishops sharing the same
coloured squares.  So maybe the current code testing bishop colours needs to 
be removed?  Though if there are three white bishops and still eight white pawns
then the board is invalid, because there has been no promotion!

Review Of The Problem
=====================

I think this problem is an excellent way to enhance basic skills.  Nothing is
required beyond basic python data structures, but where the programmer is
challenged is in applying the basic data structures to solve a moderately
challenging problem.  This is what beginners need after learning the basics.

It also helps that the tests that invalidate a board are simple and, on the
whole, self-contained so it's easy to just "add another test".

The problem behaves similarly to many real-world problems.  It's easy to get
most of the tests working, but careful though reveals that there are special
cases that weren't considered, requiring a rework of the code.
