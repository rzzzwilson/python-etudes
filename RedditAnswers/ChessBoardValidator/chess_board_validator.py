"""
Code to solve a problem from ATBS mentioned in:

https://www.reddit.com/r/learnpython/comments/feny7x/how_difficult_are_the_exercises_in_automate_the/

Chess Dictionary Validator
--------------------------
In this chapter, we used the dictionary value
{'1h': 'bking', '6c': 'wqueen', '2g': 'bbishop', '5h': 'bqueen', '3e': 'wking'}
to represent a chess board. Write a function named isValidChessBoard() that takes
a dictionary argument and returns True or False depending on if the board is valid.

A valid board will have exactly one black king and exactly one white king. Each
player can only have at most 16 pieces, at most 8 pawns, and all pieces must be
on a valid space from '1a' to '8h'; that is, a piece can’t be on space '9z'. The
piece names begin with either a 'w' or 'b' to represent white or black, followed
by 'pawn', 'knight', 'bishop', 'rook', 'queen', or 'king'. This function should
detect when a bug has resulted in an improper chess board.
--------------------------

Missteps: I originally misread the "board" dictionary to be {name: square. ...}.
          Not an expensive mistake, mainly had to edit test cases.

I was going to use a computed dictionary to map square -> colour, but changed to
a function instead.
"""

# dict holding maximum numbers of each piece
max_names = {'wpawn':8, 'wrook':2, 'wknight':2, 'wbishop':2, 'wqueen':1, 'wking':1,
             'bpawn':8, 'brook':2, 'bknight':2, 'bbishop':2, 'bqueen':1, 'bking':1}


# function mapping square string ('1a') to a string ('black' or 'white')
def square_colour(square):
    # convert square string to numeric coordinates
    (row, col) = square
    row = int(row) - 1      # both coords zero based
    col = 'abcdefgh'.index(col)
    if row % 2:
        return 'black' if col % 2 else 'white'
    return 'white' if col % 2 else 'black'

def isValidChessBoard(board):
    # initialize dict holding piece counters
    piece_count = {'wpawn':0, 'wrook':0, 'wknight':0, 'wbishop':0, 'wqueen':0, 'wking':0,
                   'bpawn':0, 'brook':0, 'bknight':0, 'bbishop':0, 'bqueen':0, 'bking':0}

    # initialize counters of bishop square colours
    # key tuple is (bishop colour, square colour)
    bishop_colours = {('white', 'white'): 0,
                      ('white', 'black'): 0,
                      ('black', 'white'): 0,
                      ('black', 'black'): 0}

    for (square, name) in board.items():
        # check that the square is valid
        try:
            (row, col) = square
        except ValueError:
            return False
        if row not in '12345678' or col not in 'abcdefgh':
            return False

        # check that the piece name is valid
        if name not in piece_count:
            return False

        # update count of times we've seen this piece name
        piece_count[name] += 1
        if piece_count[name] > max_names[name]:
            return False

        # check bishop square colour is OK
        if name in ['wbishop', 'bbishop']:
            sq_colour = square_colour(square)   # returns 'white' or 'black'
            p_colour = name[0]                  # get piece colour
            p_colour = 'white' if p_colour == 'w' else 'black'
            if bishop_colours[(p_colour, sq_colour)] > 0:
                return False
            bishop_colours[(p_colour, sq_colour)] = 1

    return True
