import unittest
from chess_board_validator import isValidChessBoard

class TestisValidChessBoard(unittest.TestCase):

    def test_empty(self):
        """Check that an empty board passes."""

        board = {}
        result = isValidChessBoard(board)
        msg = "Empty board validated False?"
        self.assertTrue(result, msg)

    def test_valid(self):
        """Check that a non-empty valid board passes."""

        board = {'2a':'wpawn'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board validated False?"
        self.assertTrue(result, msg)

    def test_bad_square(self):
        """Check that a non-empty invalid board doesn't pass (bad square)."""

        board = {'9a':'wpawn'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with bad square validated True?"
        self.assertFalse(result, msg)

    def test_bad_name(self):
        """Check that a non-empty invalid board doesn't pass (bad piece name)."""

        board = {'2a':'qpawn'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with bad piece name validated True?"
        self.assertFalse(result, msg)

    def test_too_many_knights(self):
        """Check that a non-empty invalid board doesn't pass (too many knights)."""

        board = {'2a':'qpawn', '1a':'wknight', '2c':'wknight', '3c':'wknight'}
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with too many knights validated True?"
        self.assertFalse(result, msg)

    def test_same_bishop_square_colour(self):
        """Check that a non-empty invalid board doesn't pass (two bishops, same square colour)."""

        board = {'1a':'bbishop', '8h':'bbishop'}    # both on same diagonal
        result = isValidChessBoard(board)
        msg = "Non-empty valid board with two bishops same square colour validated True?"
        self.assertFalse(result, msg)

unittest.main()
